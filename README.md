# Task Manager v.16 (client-server application based on SOAP web services)
## Software
+ Java 8
+ Maven
+ JRE
+ PostgreSQ
+ Junit
+ Hibernate
+ Hazelcast
+ Liquibase
## Developer
Mikhail Smirnov

email: mdsmirnovbox@gmail.com

## Build application
```
mvn clean
```
```
mvn install
```

## Run client side of application
```
java -jar tm-client/target/taskmanager/bin/client.jar
```

## Run server side of application
```
java -jar tm-server/target/taskmanager/bin/server.jar
```
