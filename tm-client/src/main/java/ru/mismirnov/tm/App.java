package ru.mismirnov.tm;

import org.jetbrains.annotations.NotNull;
import ru.mismirnov.tm.context.Bootstrap;

public final class App {

    public static void main(String[] args) throws Exception {
        final @NotNull Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }
}
