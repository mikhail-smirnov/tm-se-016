package ru.mismirnov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.IProjectEndpoint;
import ru.mismirnov.tm.api.endpoint.ISessionEndpoint;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.api.endpoint.IUserEndpoint;
import ru.mismirnov.tm.api.state.IStateService;

public interface ServiceLocator {

    @NotNull
    public IProjectEndpoint getProjectEndpoint();

    @NotNull
    public ITaskEndpoint getTaskEndpoint();

    @NotNull
    public IUserEndpoint getUserEndpoint();

    @NotNull
    public ISessionEndpoint getSessionEndpoint();

    @NotNull
    public ITerminalService getTerminalService();


    @NotNull
    public IStateService getStateService();
}

