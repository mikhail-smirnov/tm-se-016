package ru.mismirnov.tm.api;

import org.jetbrains.annotations.Nullable;

public interface TokenLocator {

    @Nullable
    String getToken();

    void setToken(@Nullable String token);
}
