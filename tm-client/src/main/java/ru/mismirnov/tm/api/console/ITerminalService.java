package ru.mismirnov.tm.api.console;

import org.jetbrains.annotations.Nullable;

public interface ITerminalService {

    @Nullable
    String readLine();

    int readNumber();
}