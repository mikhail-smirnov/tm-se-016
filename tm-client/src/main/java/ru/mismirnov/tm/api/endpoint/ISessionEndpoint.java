package ru.mismirnov.tm.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-06-01T20:51:32.734+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.api.tm.mismirnov.ru/", name = "ISessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ISessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.tm.mismirnov.ru/ISessionEndpoint/removeRequest", output = "http://endpoint.api.tm.mismirnov.ru/ISessionEndpoint/removeResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.mismirnov.ru/ISessionEndpoint/remove/Fault/Exception")})
    @RequestWrapper(localName = "remove", targetNamespace = "http://endpoint.api.tm.mismirnov.ru/", className = "ru.mismirnov.tm.api.endpoint.Remove")
    @ResponseWrapper(localName = "removeResponse", targetNamespace = "http://endpoint.api.tm.mismirnov.ru/", className = "ru.mismirnov.tm.api.endpoint.RemoveResponse")
    public void remove(
            @WebParam(name = "arg0", targetNamespace = "")
                    java.lang.String arg0
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.mismirnov.ru/ISessionEndpoint/getTokenRequest", output = "http://endpoint.api.tm.mismirnov.ru/ISessionEndpoint/getTokenResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.mismirnov.ru/ISessionEndpoint/getToken/Fault/Exception")})
    @RequestWrapper(localName = "getToken", targetNamespace = "http://endpoint.api.tm.mismirnov.ru/", className = "ru.mismirnov.tm.api.endpoint.GetToken")
    @ResponseWrapper(localName = "getTokenResponse", targetNamespace = "http://endpoint.api.tm.mismirnov.ru/", className = "ru.mismirnov.tm.api.endpoint.GetTokenResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.lang.String getToken(
            @WebParam(name = "arg0", targetNamespace = "")
                    java.lang.String arg0,
            @WebParam(name = "arg1", targetNamespace = "")
                    java.lang.String arg1
    ) throws Exception_Exception;
}
