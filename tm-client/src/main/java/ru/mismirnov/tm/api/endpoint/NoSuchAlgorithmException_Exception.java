package ru.mismirnov.tm.api.endpoint;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-05-20T09:53:27.061+03:00
 * Generated source version: 3.2.7
 */

@WebFault(name = "NoSuchAlgorithmException", targetNamespace = "http://endpoint.api.tm.mismirnov.ru/")
public class NoSuchAlgorithmException_Exception extends java.lang.Exception {

    private ru.mismirnov.tm.api.endpoint.NoSuchAlgorithmException noSuchAlgorithmException;

    public NoSuchAlgorithmException_Exception() {
        super();
    }

    public NoSuchAlgorithmException_Exception(String message) {
        super(message);
    }

    public NoSuchAlgorithmException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public NoSuchAlgorithmException_Exception(String message, ru.mismirnov.tm.api.endpoint.NoSuchAlgorithmException noSuchAlgorithmException) {
        super(message);
        this.noSuchAlgorithmException = noSuchAlgorithmException;
    }

    public NoSuchAlgorithmException_Exception(String message, ru.mismirnov.tm.api.endpoint.NoSuchAlgorithmException noSuchAlgorithmException, java.lang.Throwable cause) {
        super(message, cause);
        this.noSuchAlgorithmException = noSuchAlgorithmException;
    }

    public ru.mismirnov.tm.api.endpoint.NoSuchAlgorithmException getFaultInfo() {
        return this.noSuchAlgorithmException;
    }
}
