package ru.mismirnov.tm.api.state;

import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.command.AbstractCommand;

import java.util.List;

public interface IStateService {

    void setCommand(final String commandDescription, final AbstractCommand abstractCommand);

    @Nullable
    AbstractCommand getCommand(final @Nullable String command);

    @Nullable
    List<AbstractCommand> getCommands();
}
