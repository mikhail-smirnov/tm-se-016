package ru.mismirnov.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.ServiceLocator;
import ru.mismirnov.tm.api.TokenLocator;

import java.io.File;

@Setter
@Getter
public abstract class AbstractCommand {

    public static final String FILE_LOCATION = System.getProperty("user.dir") + File.separator
            + "target" + File.separator + "task-manager" + File.separator + "data" + File.separator;
    protected static final @NotNull String OPERATION_DONE = "DONE";
    protected static final @NotNull String ENTER_PROJECT_NAME = "Enter project name >>>";
    protected static final @NotNull String ENTER_TASK_NAME = "Enter task name >>>";
    protected static final @NotNull String ENTER_DESCRIPTION_NAME = "Enter description name >>>";
    protected static final @NotNull String ENTER_BEGIN_DATE = "Enter begin date in the following format: DD.MM.YYYY >>> ";
    protected static final @NotNull String ENTER_END_DATE = "Enter end date in the following format: DD.MM.YYYY >>> ";
    protected static final @NotNull String LINE_SEPARATOR = System.getProperty("line.separator");
    protected static final @NotNull String RE_LOGIN = "Please re-login";
    protected @NotNull ServiceLocator serviceLocator;

    protected @NotNull TokenLocator tokenLocator;

    @Nullable
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;
}