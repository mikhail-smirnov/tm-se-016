package ru.mismirnov.tm.command.common;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.mismirnov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Show Project Build Info";
    }

    @Override
    public void execute() {
        @NotNull final String buildNumber = Manifests.read("buildNumber");
        @NotNull final String developer = Manifests.read("developer");
        System.out.println("build number: " + buildNumber + "\n" + "developer: " + developer);
    }
}

