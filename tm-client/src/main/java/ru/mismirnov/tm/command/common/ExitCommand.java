package ru.mismirnov.tm.command.common;

import org.jetbrains.annotations.NotNull;
import ru.mismirnov.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Close program";
    }

    @Override
    public void execute() {
    }
}
