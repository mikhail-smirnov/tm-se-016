package ru.mismirnov.tm.command.common;

import org.jetbrains.annotations.NotNull;
import ru.mismirnov.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "List of commands";
    }

    @Override
    public void execute() {
        for (final @NotNull AbstractCommand command :
                serviceLocator.getStateService().getCommands()) {
            System.out.println(command.command() + ": "
                    + command.description());
        }
    }
}
