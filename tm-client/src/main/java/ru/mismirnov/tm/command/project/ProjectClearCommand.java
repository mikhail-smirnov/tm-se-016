package ru.mismirnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.IProjectEndpoint;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        projectEndpoint.removeAllProjects(token);
        System.out.println(OPERATION_DONE);
    }
}
