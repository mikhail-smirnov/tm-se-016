package ru.mismirnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.IProjectEndpoint;
import ru.mismirnov.tm.api.endpoint.ProjectDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;
import ru.mismirnov.tm.util.DateUtil;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Add new project";
    }

    @Override
    public void execute() throws Exception {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        System.out.print(ENTER_PROJECT_NAME);
        @Nullable String projectName = terminalService.readLine();
        System.out.print(ENTER_DESCRIPTION_NAME);
        @Nullable String description = terminalService.readLine();
        System.out.print(ENTER_BEGIN_DATE);
        @Nullable Date beginDate = DateUtil.formatDate(terminalService.readLine());
        System.out.print(ENTER_END_DATE);
        @Nullable Date endDate = DateUtil.formatDate(terminalService.readLine());
        final @NotNull ProjectDto project = new ProjectDto();
        project.setName(projectName);
        project.setDescription(description);
        project.setBeginDate(DateUtil.toXMLGregorianCalendar(beginDate));
        project.setEndDate(DateUtil.toXMLGregorianCalendar(endDate));
        projectEndpoint.persistProject(token, project);
        System.out.println(OPERATION_DONE);
    }
}
