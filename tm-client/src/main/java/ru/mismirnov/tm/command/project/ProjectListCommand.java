package ru.mismirnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.endpoint.*;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

import java.util.List;

import static ru.mismirnov.tm.util.EntityUtil.showProjectInfo;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final @NotNull ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final @Nullable List<ProjectDto> projects = projectEndpoint.findAllProjects(token);
        if (projects == null || projects.isEmpty()) return;
        final @Nullable List<TaskDto> tasks = taskEndpoint.findAllTasks(token);
        System.out.println(showProjectInfo(projects, tasks));
    }
}
