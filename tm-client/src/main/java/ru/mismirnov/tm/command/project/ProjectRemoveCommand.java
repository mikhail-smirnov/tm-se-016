package ru.mismirnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.*;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        System.out.print(ENTER_PROJECT_NAME);
        final @Nullable String projectName = terminalService.readLine();
        final @Nullable List<ProjectDto> projects = projectEndpoint.findAllProjects(token);
        final @NotNull ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        if (projects == null || projects.isEmpty()) return;
        final @NotNull ProjectDto projectToRemove = projects.stream().filter(project -> project.getName().contains(projectName))
                .findFirst().orElse(new ProjectDto());
        final @Nullable List<TaskDto> tasks = taskEndpoint.findAllTasks(token);
        if (tasks == null || tasks.isEmpty()) return;
        final @NotNull List<TaskDto> tasksToRemove = new ArrayList<>();
        for (@NotNull TaskDto task : tasks) {
            if (task.getProjectId().equals(projectToRemove.getId())) {
                tasksToRemove.add(task);
            }
        }
        serviceLocator.getTaskEndpoint().removeTasks(token, tasksToRemove);
        serviceLocator.getProjectEndpoint().removeProject(token, projectToRemove.getId());
        System.out.println(OPERATION_DONE);
    }
}
