package ru.mismirnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.IProjectEndpoint;
import ru.mismirnov.tm.api.endpoint.ProjectDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

import static ru.mismirnov.tm.util.EntityUtil.showProjectInfo;

public final class ProjectSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-search";
    }

    @NotNull
    @Override
    public String description() {
        return "Search project by its attributes";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        final @NotNull IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final @NotNull StringBuilder searchChoice = new StringBuilder();
        searchChoice.append("Choose (number) by which attribute project should be found: ").append(LINE_SEPARATOR)
                .append("1. By name").append(LINE_SEPARATOR)
                .append("2. description").append(LINE_SEPARATOR);
        System.out.println(searchChoice.toString());
        switch (terminalService.readNumber()) {
            case 1:
                System.out.print("Enter project name >>> ");
                @Nullable final String projectName = terminalService.readLine();
                ProjectDto projectByName = projectEndpoint.findProjectByName(token, projectName).stream().findFirst().orElse(null);
                if (projectByName == null) return;
                System.out.println(showProjectInfo(projectByName));
                break;
            case 2:
                System.out.print("Enter project description >>> ");
                @Nullable final String projectDescription = terminalService.readLine();
                ProjectDto projectByDescription = projectEndpoint.findProjectByDescription(token, projectDescription).stream().findFirst().orElse(null);
                if (projectByDescription == null) return;
                System.out.println(showProjectInfo(projectByDescription));
                break;
            default:
                System.out.println("Operation is undefined");
                break;
        }
    }
}
