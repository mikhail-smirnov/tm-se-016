package ru.mismirnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.IProjectEndpoint;
import ru.mismirnov.tm.api.endpoint.ProjectDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;
import ru.mismirnov.tm.util.EntityUtil;

public final class ProjectSetStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-setstatus";
    }

    @NotNull
    @Override
    public String description() {
        return "Change project status";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        final @NotNull IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        System.out.print("Enter project name >>> ");
        final @Nullable String projectName = terminalService.readLine();
        System.out.print("Enter current project status >>> ");
        final @Nullable String status = terminalService.readLine();
        ProjectDto project = projectEndpoint.findProjectByName(token, projectName).stream().findFirst().orElse(null);
        if (project == null) return;
        project.setStatus(EntityUtil.getStatusFromString(status));
        projectEndpoint.mergeProject(token, project);
        System.out.println("Status has been changed");
    }
}

