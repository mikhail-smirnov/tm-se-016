package ru.mismirnov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.IProjectEndpoint;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.api.endpoint.ProjectDto;
import ru.mismirnov.tm.api.endpoint.TaskDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

import java.util.List;

import static ru.mismirnov.tm.util.EntityUtil.showProjectInfo;

public final class ProjectSortCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-sort";
    }

    @NotNull
    @Override
    public String description() {
        return "Sort projects in specified order";
    }

    @Override
    public void execute() throws Exception {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final @NotNull ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        final @NotNull StringBuilder sortChoice = new StringBuilder();
        sortChoice.append("Choose (number) in which way projects have to be sorted: ").append(LINE_SEPARATOR)
                .append("1. By creation date").append(LINE_SEPARATOR)
                .append("2. By project begin date").append(LINE_SEPARATOR)
                .append("3. By project end date").append(LINE_SEPARATOR)
                .append("4. By project status");
        System.out.println(sortChoice);

        @Nullable List<TaskDto> tasks = taskEndpoint.findAllTasks(token);
        switch (terminalService.readNumber()) {
            case 1:
                final @Nullable List<ProjectDto> projectsSortedByCreationDate = projectEndpoint.findAllProjectsSorted(token, "creationDate");
                if (projectsSortedByCreationDate == null) return;
                System.out.println(showProjectInfo(projectsSortedByCreationDate, tasks));
                break;
            case 2:
                final @Nullable List<ProjectDto> projectsSortedByBeginDate = projectEndpoint.findAllProjectsSorted(token, "beginDate");
                if (projectsSortedByBeginDate == null) return;
                System.out.println(showProjectInfo(projectsSortedByBeginDate, tasks));
                break;
            case 3:
                final @Nullable List<ProjectDto> projectsSortedByEndDate = projectEndpoint.findAllProjectsSorted(token, "endDate");
                if (projectsSortedByEndDate == null) return;
                System.out.println(showProjectInfo(projectsSortedByEndDate, tasks));
                break;
            case 4:
                final @Nullable List<ProjectDto> projectsSortedByStatus = projectEndpoint.findAllProjectsSorted(token, "status");
                if (projectsSortedByStatus == null) return;
                System.out.println(showProjectInfo(projectsSortedByStatus, tasks));
                break;
            default:
                System.out.println("Operation is undefined");
                break;
        }
    }
}

