package ru.mismirnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.*;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

public final class TaskAssignCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-assign";
    }

    @NotNull
    @Override
    public String description() {
        return "Assign task to project";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final @NotNull IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        System.out.print(ENTER_PROJECT_NAME);
        final @Nullable String projectName = terminalService.readLine();
        System.out.print(ENTER_TASK_NAME);
        final @Nullable String taskName = terminalService.readLine();
        final @Nullable TaskDto task = taskEndpoint.findTaskByName(token, taskName).stream().findFirst().orElse(null);
        if (task == null) return;
        final @Nullable ProjectDto project = projectEndpoint.findProjectByName(token, projectName).stream().findFirst().orElse(null);
        if (project == null) return;
        task.setProjectId(project.getId());
        taskEndpoint.mergeTask(token, task);
        System.out.println(OPERATION_DONE);
    }
}
