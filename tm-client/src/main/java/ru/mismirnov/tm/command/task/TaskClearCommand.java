package ru.mismirnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        serviceLocator.getTaskEndpoint().removeAllTasks(token);
        System.out.println(OPERATION_DONE);
    }
}
