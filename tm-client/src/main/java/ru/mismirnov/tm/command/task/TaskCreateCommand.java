package ru.mismirnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.api.endpoint.TaskDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;
import ru.mismirnov.tm.util.DateUtil;

import java.util.Date;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Add new task";
    }

    @Override
    public void execute() throws Exception {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        System.out.print(ENTER_TASK_NAME);
        final @Nullable String taskName = terminalService.readLine();
        System.out.print(ENTER_DESCRIPTION_NAME);
        final @Nullable String description = terminalService.readLine();
        System.out.print(ENTER_BEGIN_DATE);
        final @NotNull Date beginDate = DateUtil.formatDate(terminalService.readLine());
        System.out.print(ENTER_END_DATE);
        final @NotNull Date endDate = DateUtil.formatDate(terminalService.readLine());
        final @NotNull TaskDto task = new TaskDto();
        task.setName(taskName);
        task.setDescription(description);
        task.setBeginDate(DateUtil.toXMLGregorianCalendar(beginDate));
        task.setEndDate(DateUtil.toXMLGregorianCalendar(endDate));
        taskEndpoint.persistTask(token, task);
        System.out.println(OPERATION_DONE);
    }
}
