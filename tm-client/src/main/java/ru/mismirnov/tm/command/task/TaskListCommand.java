package ru.mismirnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.api.endpoint.TaskDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

import java.util.List;

import static ru.mismirnov.tm.util.EntityUtil.showTaskInfo;

public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "List all projects";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final @Nullable List<TaskDto> tasks = taskEndpoint.findAllTasks(token);
        if (tasks == null || tasks.isEmpty()) return;
        System.out.println(showTaskInfo(tasks));
    }
}
