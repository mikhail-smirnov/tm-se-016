package ru.mismirnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.api.endpoint.TaskDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

import java.util.List;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        System.out.print(ENTER_TASK_NAME);
        final @Nullable String taskName = terminalService.readLine();
        final @Nullable List<TaskDto> tasks = taskEndpoint.findAllTasks(token);
        if (tasks == null || tasks.isEmpty()) return;
        final @NotNull TaskDto taskToRemove = tasks.stream().filter(task -> task.getName().contains(taskName))
                .findFirst().orElse(new TaskDto());
        taskEndpoint.removeTask(token, taskToRemove.getId());
        System.out.println(OPERATION_DONE);
    }
}
