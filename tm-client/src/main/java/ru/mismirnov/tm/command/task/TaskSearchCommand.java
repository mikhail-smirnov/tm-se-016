package ru.mismirnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.api.endpoint.TaskDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

import static ru.mismirnov.tm.util.EntityUtil.showTaskInfo;

public final class TaskSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-search";
    }

    @NotNull
    @Override
    public String description() {
        return "Search task by its attributes";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        final @NotNull ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final @NotNull StringBuilder searchChoice = new StringBuilder();
        searchChoice.append("Choose (number) by which attribute task should be found: ").append(LINE_SEPARATOR)
                .append("1. By name").append(LINE_SEPARATOR)
                .append("2. description").append(LINE_SEPARATOR);
        System.out.println(searchChoice.toString());
        switch (terminalService.readNumber()) {
            case 1:
                System.out.print("Enter task name >>> ");
                @Nullable final String taskName = terminalService.readLine();
                TaskDto taskByName = taskEndpoint.findTaskByName(token, taskName).stream().findFirst().orElse(null);
                if (taskByName == null) return;
                System.out.println(showTaskInfo(taskByName));
                break;
            case 2:
                System.out.print("Enter task description >>> ");
                @Nullable final String projectDescription = terminalService.readLine();
                TaskDto taskByDescription = taskEndpoint.findTaskByDescription(token, projectDescription).stream().findFirst().orElse(null);
                if (taskByDescription == null) return;
                System.out.println(showTaskInfo(taskByDescription));
                break;
            default:
                System.out.println("Operation is undefined");
                break;
        }
    }
}

