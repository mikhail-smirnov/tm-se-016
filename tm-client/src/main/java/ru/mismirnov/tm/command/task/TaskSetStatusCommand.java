package ru.mismirnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.api.endpoint.TaskDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;
import ru.mismirnov.tm.util.EntityUtil;


public final class TaskSetStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-setstatus";
    }

    @NotNull
    @Override
    public String description() {
        return "Change project status";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        System.out.print("Enter task name >>> ");
        @Nullable final String taskName = terminalService.readLine();
        System.out.print("Enter current task status >>> ");
        @Nullable final String status = terminalService.readLine();
        TaskDto task = taskEndpoint.findTaskByName(token, taskName).stream().findFirst().orElse(null);
        if (task == null) return;
        task.setStatus(EntityUtil.getStatusFromString(status));
        taskEndpoint.mergeTask(token, task);
        System.out.println("Status has been changed");
    }
}
