package ru.mismirnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.api.endpoint.TaskDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

import java.util.List;

import static ru.mismirnov.tm.util.EntityUtil.showTaskInfo;

public final class TaskSortCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-sort";
    }

    @NotNull
    @Override
    public String description() {
        return "Sort tasks in specified order";
    }

    @Override
    public void execute() throws Exception {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull StringBuilder sortChoice = new StringBuilder();
        sortChoice.append("Choose (number) in which way tasks have to be sorted: ").append(LINE_SEPARATOR)
                .append("1. By creation date").append(LINE_SEPARATOR)
                .append("2. By task start date").append(LINE_SEPARATOR)
                .append("3. By task end date").append(LINE_SEPARATOR)
                .append("4. By task status");
        System.out.println(sortChoice);
        switch (terminalService.readNumber()) {
            case 1:
                final @Nullable List<TaskDto> tasksSortedByCreationDate = taskEndpoint.findAllTasksSorted(token, "creationDate");
                if (tasksSortedByCreationDate == null) return;
                System.out.println(showTaskInfo(tasksSortedByCreationDate));
                break;
            case 2:
                final @Nullable List<TaskDto> tasksSortedByBeginDate = taskEndpoint.findAllTasksSorted(token, "beginDate");
                if (tasksSortedByBeginDate == null) return;
                System.out.println(showTaskInfo(tasksSortedByBeginDate));
                break;
            case 3:
                final @Nullable List<TaskDto> tasksSortedByEndDate = taskEndpoint.findAllTasksSorted(token, "endDate");
                if (tasksSortedByEndDate == null) return;
                System.out.println(showTaskInfo(tasksSortedByEndDate));
                break;
            case 4:
                final @Nullable List<TaskDto> tasksSortedByStatus = taskEndpoint.findAllTasksSorted(token, "status");
                if (tasksSortedByStatus == null) return;
                System.out.println(showTaskInfo(tasksSortedByStatus));
                break;
            default:
                System.out.println("Operation is undefined");
                break;
        }
    }
}

