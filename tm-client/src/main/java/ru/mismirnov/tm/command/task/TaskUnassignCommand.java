package ru.mismirnov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.api.endpoint.TaskDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

public final class TaskUnassignCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-unassign";
    }

    @NotNull
    @Override
    public String description() {
        return "Assign task to project";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        System.out.print(ENTER_PROJECT_NAME);
        final @Nullable String projectName = terminalService.readLine();
        System.out.print(ENTER_TASK_NAME);
        final @Nullable String taskName = terminalService.readLine();
        final @Nullable TaskDto task = taskEndpoint.findTaskByName(token, taskName).stream().findFirst().orElse(null);
        if (task == null) return;
        task.setProjectId(null);
        taskEndpoint.mergeTask(token, task);
        System.out.println(OPERATION_DONE);
    }
}
