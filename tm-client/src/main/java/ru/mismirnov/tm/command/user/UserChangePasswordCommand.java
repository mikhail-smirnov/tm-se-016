package ru.mismirnov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.IUserEndpoint;
import ru.mismirnov.tm.api.endpoint.UserDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;
import ru.mismirnov.tm.util.PasswordHashUtil;

import java.security.NoSuchAlgorithmException;

public final class UserChangePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-changepassword";
    }

    @NotNull
    @Override
    public String description() {
        return "Change password";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException, NoSuchAlgorithmException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final UserDto user = userEndpoint.findOneUser(token);
        System.out.print("Enter old password >>> ");
        while (!PasswordHashUtil.getMD5(terminalService.readLine()).equals(user.getPassword())) {
            System.out.println("Incorrect password");
        }
        System.out.print("Enter new password >>> ");
        final @Nullable String password = terminalService.readLine();
        if (password == null) return;
        user.setPassword(password);
        userEndpoint.mergeUser(token, user);
        System.out.print("Password has been changed");
    }
}
