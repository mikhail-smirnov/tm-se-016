package ru.mismirnov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.IUserEndpoint;
import ru.mismirnov.tm.api.endpoint.Role;
import ru.mismirnov.tm.api.endpoint.UserDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

import java.security.NoSuchAlgorithmException;

import static ru.mismirnov.tm.util.PasswordHashUtil.getMD5;

public final class UserCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-register";
    }

    @NotNull
    @Override
    public String description() {
        return "User registration";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException, NoSuchAlgorithmException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        System.out.print("Enter user name >>> ");
        final @Nullable String userName = terminalService.readLine();
        System.out.print("Enter login >>> ");
        final @Nullable String userLogin = terminalService.readLine();
        System.out.print("Enter password >>> ");
        final @Nullable String userPassword = terminalService.readLine();
        if (userPassword == null) return;
        final @NotNull UserDto user = new UserDto();
        user.setName(userName);
        user.setLogin(userLogin);
        user.setPassword(getMD5(userPassword));
        user.setRole(Role.USER);
        userEndpoint.persistUser(token, user);
        System.out.println("OK");
    }
}
