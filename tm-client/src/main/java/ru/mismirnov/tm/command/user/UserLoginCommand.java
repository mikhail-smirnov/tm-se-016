package ru.mismirnov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.command.AbstractCommand;

public final class UserLoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Log in";
    }

    @Override
    public void execute() throws Exception_Exception {
        final @NotNull ITerminalService terminalService = serviceLocator.getTerminalService();
        System.out.print("Enter user login >>> ");
        final @Nullable String userLogin = terminalService.readLine();
        System.out.print("Enter password >>> ");
        final @Nullable String userPassword = terminalService.readLine();
        if (userPassword == null) return;
        final @Nullable String token = serviceLocator.getSessionEndpoint().getToken(userLogin, userPassword);
        if (token == null) return;
        tokenLocator.setToken(token);
        System.out.println("You have been logged in");
    }
}
