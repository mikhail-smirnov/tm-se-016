package ru.mismirnov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

public final class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Log out";
    }

    @Override
    public void execute() throws Exception_Exception, AccessForbiddenException {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        serviceLocator.getSessionEndpoint().remove(token);
        tokenLocator.setToken(null);
        System.out.println("You have been logged out");
    }
}
