package ru.mismirnov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.endpoint.IUserEndpoint;
import ru.mismirnov.tm.api.endpoint.UserDto;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.exception.AccessForbiddenException;

import static ru.mismirnov.tm.util.EntityUtil.showUserProfileInfo;

public final class UserShowProfile extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-showprofile";
    }

    @NotNull
    @Override
    public String description() {
        return "Show user profile";
    }

    @Override
    public void execute() throws Exception {
        final @Nullable String token = tokenLocator.getToken();
        if (token == null) throw new AccessForbiddenException();
        final @NotNull IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        final @Nullable UserDto currentUser = userEndpoint.findOneUser(token);
        if (currentUser == null) return;
        System.out.println(showUserProfileInfo(currentUser));
    }
}
