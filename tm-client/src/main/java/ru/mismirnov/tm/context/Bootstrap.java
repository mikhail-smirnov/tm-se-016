package ru.mismirnov.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.mismirnov.tm.api.ServiceLocator;
import ru.mismirnov.tm.api.TokenLocator;
import ru.mismirnov.tm.api.console.ITerminalService;
import ru.mismirnov.tm.api.endpoint.IProjectEndpoint;
import ru.mismirnov.tm.api.endpoint.ISessionEndpoint;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.api.endpoint.IUserEndpoint;
import ru.mismirnov.tm.api.state.IStateService;
import ru.mismirnov.tm.command.AbstractCommand;
import ru.mismirnov.tm.endpoint.ProjectEndpointService;
import ru.mismirnov.tm.endpoint.SessionEndpointService;
import ru.mismirnov.tm.endpoint.TaskEndpointService;
import ru.mismirnov.tm.endpoint.UserEndpointService;
import ru.mismirnov.tm.exception.CommandCorruptException;
import ru.mismirnov.tm.exception.EntityDuplicateException;
import ru.mismirnov.tm.service.StateService;
import ru.mismirnov.tm.service.TerminalService;

import java.security.NoSuchAlgorithmException;
import java.util.Scanner;
import java.util.Set;

@Setter
@Getter
public final class Bootstrap implements ServiceLocator, TokenLocator {

    private static final @NotNull String LINE_SEPARATOR = System.getProperty("line.separator");
    private final @NotNull ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
    private final @NotNull IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
    private final @NotNull ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
    private final @NotNull IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
    private final @NotNull ITerminalService terminalService = new TerminalService();
    private final @NotNull IStateService stateService = new StateService();
    private final @NotNull Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.mismirnov.tm").getSubTypesOf(AbstractCommand.class);
    private @Nullable String token;

    private void init() throws IllegalAccessException, CommandCorruptException, InstantiationException, EntityDuplicateException, NoSuchAlgorithmException {
        registerCommands();
    }

    private void registerCommands() throws IllegalAccessException, InstantiationException, CommandCorruptException {
        for (final @NotNull Class commandClass : classes) {
            if (AbstractCommand.class.isAssignableFrom(commandClass))
                registry((AbstractCommand) commandClass.newInstance());
        }
    }


    private void registry(final AbstractCommand command) throws CommandCorruptException {
        final @NotNull String cliCommand = command.command();
        final @NotNull String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        command.setTokenLocator(this);
        stateService.setCommand(cliCommand, command);
    }

    public void start() {
        try {
            init();
            System.out.println("*** WELCOME TO TASK MANAGER ***");
            String command = "";
            final @NotNull Scanner scanner = new Scanner(System.in);
            while (!"exit".equals(command)) {
                command = scanner.nextLine();
                try {
                    execute(command);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void execute(final @NotNull String command) throws Exception {
        if (command.isEmpty()) return;
        final @Nullable AbstractCommand abstractCommand = stateService.getCommand(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }
}