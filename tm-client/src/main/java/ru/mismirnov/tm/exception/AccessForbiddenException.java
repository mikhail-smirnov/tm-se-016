package ru.mismirnov.tm.exception;

public class AccessForbiddenException extends Exception {

    public AccessForbiddenException() {
        super("Access forbidden");
    }
}
