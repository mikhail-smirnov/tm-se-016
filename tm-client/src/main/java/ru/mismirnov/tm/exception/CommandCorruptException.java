package ru.mismirnov.tm.exception;

public final class CommandCorruptException extends Exception {

    public CommandCorruptException() {
        super("Corrupt command");
    }
}
