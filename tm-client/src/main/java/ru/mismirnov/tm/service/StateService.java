package ru.mismirnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.state.IStateService;
import ru.mismirnov.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class StateService implements IStateService {


    private final @NotNull Map<String, AbstractCommand> commands = new HashMap<>();

    @Override
    public void setCommand(final @Nullable String commandDescription, final @Nullable AbstractCommand abstractCommand) {
        if (commandDescription == null || commandDescription.isEmpty()) return;
        if (abstractCommand == null) return;
        commands.put(commandDescription, abstractCommand);
    }

    @Nullable
    @Override
    public AbstractCommand getCommand(final @Nullable String command) {
        if (command == null || command.isEmpty()) return null;
        return commands.get(command);
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }
}

