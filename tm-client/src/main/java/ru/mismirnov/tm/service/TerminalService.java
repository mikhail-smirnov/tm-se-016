package ru.mismirnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.console.ITerminalService;

import java.util.Scanner;

public final class TerminalService implements ITerminalService {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);

    @Nullable
    @Override
    public String readLine() {
        return scanner.next();
    }

    @Override
    public int readNumber() {
        return scanner.nextInt();
    }
}

