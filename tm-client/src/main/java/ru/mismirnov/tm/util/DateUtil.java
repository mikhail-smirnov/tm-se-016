package ru.mismirnov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DateUtil {

    private static final @NotNull SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static Date formatDate(final @NotNull String date) throws ParseException {
        return formatter.parse(date);
    }

    @Nullable
    public static XMLGregorianCalendar toXMLGregorianCalendar(@NotNull final Date date) throws Exception {
        GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        XMLGregorianCalendar xmlCalendar = null;
        xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        return xmlCalendar;
    }

}

