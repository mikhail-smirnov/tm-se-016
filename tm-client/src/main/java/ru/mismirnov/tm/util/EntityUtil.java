package ru.mismirnov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.endpoint.ProjectDto;
import ru.mismirnov.tm.api.endpoint.Status;
import ru.mismirnov.tm.api.endpoint.TaskDto;
import ru.mismirnov.tm.api.endpoint.UserDto;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.List;

public final class EntityUtil {

    private static final @NotNull String LINE_SEPARATOR = System.getProperty("line.separator");

    @Nullable
    public static String showProjectInfo(final @Nullable List<ProjectDto> projects, final @Nullable List<TaskDto> tasks) {
        if (projects == null || projects.isEmpty()) return null;
        int counter = 0;
        final @NotNull StringBuilder projectInfo = new StringBuilder();
        for (@NotNull ProjectDto project : projects) {
            @Nullable XMLGregorianCalendar beginDate = project.getBeginDate();
            @Nullable XMLGregorianCalendar endDate = project.getEndDate();
            if (beginDate == null) return null;
            if (endDate == null) return null;
            projectInfo.append(++counter).append(". ").append(project.getName())
                    .append(" Description: ").append(project.getDescription())
                    .append(" Status: ").append(project.getStatus())
                    .append(" Begin Date: ").append(beginDate.toString())
                    .append(" End Date: ").append(endDate.toString())
                    .append(LINE_SEPARATOR).append(" ### Project Tasks ### ").append(LINE_SEPARATOR).append(showProjectTaskInfo(project, tasks)).append(LINE_SEPARATOR);
        }
        return projectInfo.toString();
    }

    @Nullable
    public static String showProjectInfo(final ProjectDto project) {
        if (project == null) return null;
        int counter = 0;
        final @NotNull StringBuilder projectInfo = new StringBuilder();
        @Nullable XMLGregorianCalendar beginDate = project.getBeginDate();
        @Nullable XMLGregorianCalendar endDate = project.getEndDate();
        if (beginDate == null) return null;
        if (endDate == null) return null;
        projectInfo.append(++counter).append(". ").append(project.getName())
                .append(" Description: ").append(project.getDescription())
                .append(" Status: ").append(project.getStatus())
                .append(" Begin Date: ").append(beginDate.toString())
                .append(" End Date: ").append(endDate.toString())
                .append(LINE_SEPARATOR);
        return projectInfo.toString();
    }

    @Nullable
    public static String showUserProfileInfo(final @Nullable UserDto user) {
        if (user == null) return null;
        int counter = 0;
        final @NotNull StringBuilder profileInfo = new StringBuilder();
        profileInfo.append("*** USER PROFILE ***").append(LINE_SEPARATOR)
                .append("User name: ").append(user.getName()).append(LINE_SEPARATOR)
                .append("User login: ").append(user.getLogin()).append(LINE_SEPARATOR);
        return profileInfo.toString();
    }

    @Nullable
    public static String showTaskInfo(final @Nullable List<TaskDto> tasks) {
        if (tasks == null || tasks.isEmpty()) return null;
        int counter = 0;
        StringBuilder projectInfo = new StringBuilder();
        for (@NotNull TaskDto task : tasks) {
            @Nullable XMLGregorianCalendar beginDate = task.getBeginDate();
            @Nullable XMLGregorianCalendar endDate = task.getEndDate();
            if (beginDate == null) return null;
            if (endDate == null) return null;
            projectInfo.append(++counter).append(". ").append(task.getName())
                    .append(" Description: ").append(task.getDescription())
                    .append(" Begin Date: ").append(beginDate.toString())
                    .append(" End Date: ").append(endDate.toString())
                    .append(LINE_SEPARATOR);
        }
        return projectInfo.toString();
    }

    @Nullable
    public static String showTaskInfo(final TaskDto task) {
        if (task == null) return null;
        int counter = 0;
        StringBuilder taskInfo = new StringBuilder();
        @Nullable XMLGregorianCalendar beginDate = task.getBeginDate();
        @Nullable XMLGregorianCalendar endDate = task.getEndDate();
        if (beginDate == null) return null;
        if (endDate == null) return null;
        taskInfo.append(++counter).append(". ").append(task.getName())
                .append(" Description: ").append(task.getDescription())
                .append(" Begin Date: ").append(beginDate.toString())
                .append(" End Date: ").append(endDate.toString())
                .append(LINE_SEPARATOR);
        return taskInfo.toString();
    }

    @Nullable
    private static String showProjectTaskInfo(final @NotNull ProjectDto project, final @Nullable List<TaskDto> tasksList) {
        final @NotNull StringBuilder tasks = new StringBuilder();
        if (tasksList.isEmpty() || tasksList == null) {
            return tasks.append("This project has no tasks at this moment").toString();
        }
        int counter = 0;
        for (@NotNull TaskDto task : tasksList) {
            @Nullable String projectId = project.getId();
            if (projectId == null) return null;
            if (projectId.equals(task.getProjectId())) {
                @Nullable XMLGregorianCalendar beginDate = task.getBeginDate();
                @Nullable XMLGregorianCalendar endDate = task.getEndDate();
                if (beginDate == null) return null;
                if (endDate == null) return null;
                tasks.append(++counter)
                        .append(". ")
                        .append(task.getName())
                        .append(" Description: ").append(task.getDescription())
                        .append(" Begin Date: ").append(beginDate.toString())
                        .append(" End Date: ").append(endDate.toString())
                        .append(LINE_SEPARATOR);
            }
        }
        return tasks.append("------------------------------").toString();
    }

    public static Status getStatusFromString(final @NotNull String text) {
        @Nullable Status status = null;
        switch (text) {
            case "Запланировано":
                status = Status.SCHEDULED;
                break;
            case "В процессе выполнения":
                status = Status.IN_PROGRESS;
                break;
            case "Выполнено":
                status = Status.READY;
        }
        return status;
    }
}