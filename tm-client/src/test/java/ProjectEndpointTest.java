import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.mismirnov.tm.api.endpoint.*;
import ru.mismirnov.tm.endpoint.ProjectEndpointService;
import ru.mismirnov.tm.endpoint.SessionEndpointService;
import ru.mismirnov.tm.util.DateUtil;

import java.lang.Exception;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class ProjectEndpointTest {

    private @NotNull IProjectEndpoint projectEndpoint;
    private @NotNull ISessionEndpoint sessionEndpoint;
    private @NotNull String token;
    private @NotNull String adminToken;

    @Before
    public void setUp() throws Exception_Exception {
        projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        token = sessionEndpoint.getToken("user", "user");
        adminToken = sessionEndpoint.getToken("admin", "admin");
    }

    @Test
    public void givenNewProject_whenAddProject_thenProjectAdded() throws Exception_Exception {
        final @NotNull ProjectDto project = new ProjectDto();
        project.setName("fffff");
        project.setDescription("dddddd");
        projectEndpoint.persistProject(token, project);
        Assert.assertNotNull(projectEndpoint.findProjectByName(token, "fffff"));
    }

    @Test
    public void givenExistingProjects_whenFindAllProjects_thenAllProjectFound() throws Exception_Exception {
        final @NotNull ProjectDto project = new ProjectDto();
        project.setName("fffff");
        project.setDescription("dddddd");
        projectEndpoint.persistProject(token, project);
        final @NotNull ProjectDto project1 = new ProjectDto();
        project1.setName("fffffs");
        project1.setDescription("dddddds");
        projectEndpoint.persistProject(token, project1);
        List<ProjectDto> projects = projectEndpoint.findAllProjects(token);
        Assert.assertTrue(projects.size() == 2);
    }

    @Test
    public void givenExistingProject_whenFindProjectByItsName_thenProjectFound() throws Exception_Exception {
        final @NotNull ProjectDto project = new ProjectDto();
        final @NotNull String projectId = UUID.randomUUID().toString();
        project.setId(projectId);
        project.setName("fffff");
        project.setDescription("dddddd");
        projectEndpoint.persistProject(token, project);
        final @NotNull ProjectDto extractedProject = projectEndpoint.findProjectByName(token, "fffff").stream().findFirst().orElse(null);
        ProjectDto foundProject = projectEndpoint.findOneProject(token, projectId);
        assertThat(foundProject).isEqualToComparingFieldByField(extractedProject);
    }

    @Test
    public void givenExistingProject_whenFindProjectByItsDescription_thenProjectFound() throws Exception_Exception {
        final @NotNull ProjectDto project = new ProjectDto();
        final @NotNull String projectId = UUID.randomUUID().toString();
        project.setId(projectId);
        project.setName("fffff");
        project.setDescription("dddddd");
        projectEndpoint.persistProject(token, project);
        final @NotNull ProjectDto extractedProject = projectEndpoint.findProjectByDescription(token, "dddddd").stream().findFirst().orElse(null);
        ProjectDto foundProject = projectEndpoint.findOneProject(token, projectId);
        assertThat(foundProject).isEqualToComparingFieldByField(extractedProject);
    }

    @Test
    public void givenExistingUnsortedProjects_whenSortProjectsByBeginDate_thenProjectsSorted() throws Exception {
        final @NotNull ProjectDto project = new ProjectDto();
        project.setName("fff");
        project.setBeginDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2015")));
        projectEndpoint.persistProject(token, project);
        final @NotNull ProjectDto project1 = new ProjectDto();
        project1.setName("ddd");
        project1.setBeginDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2023")));
        projectEndpoint.persistProject(token, project1);
        final @NotNull ProjectDto project2 = new ProjectDto();
        project2.setName("ccc");
        project2.setBeginDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2016")));
        projectEndpoint.persistProject(token, project2);
        List<ProjectDto> sortedProjects = projectEndpoint.findAllProjectsSorted(token, "beginDate");
        Assert.assertEquals("fff", sortedProjects.get(0).getName());
        Assert.assertEquals("ccc", sortedProjects.get(1).getName());
        Assert.assertEquals("ddd", sortedProjects.get(2).getName());
    }

    @Test
    public void givenExistingUnsortedProjects_whenSortProjectsByEndDate_thenProjectsSorted() throws Exception {
        final @NotNull ProjectDto project = new ProjectDto();
        project.setName("fff");
        project.setEndDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2017")));
        projectEndpoint.persistProject(token, project);
        final @NotNull ProjectDto project1 = new ProjectDto();
        project1.setName("ddd");
        project1.setEndDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2024")));
        projectEndpoint.persistProject(token, project1);
        final @NotNull ProjectDto project2 = new ProjectDto();
        project2.setName("ccc");
        project2.setEndDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2018")));
        projectEndpoint.persistProject(token, project2);
        List<ProjectDto> sortedProjects = projectEndpoint.findAllProjectsSorted(token, "endDate");
        Assert.assertEquals("fff", sortedProjects.get(0).getName());
        Assert.assertEquals("ccc", sortedProjects.get(1).getName());
        Assert.assertEquals("ddd", sortedProjects.get(2).getName());
    }

    @Test
    public void givenExistingUnsortedProjects_whenSortProjectsByCreationDate_thenProjectsSorted() throws Exception {
        final @NotNull ProjectDto project = new ProjectDto();
        project.setName("fff");
        projectEndpoint.persistProject(token, project);
        Thread.sleep(1000);
        final @NotNull ProjectDto project1 = new ProjectDto();
        project1.setName("ddd");
        projectEndpoint.persistProject(token, project1);
        Thread.sleep(1000);
        final @NotNull ProjectDto project2 = new ProjectDto();
        project2.setName("ccc");
        projectEndpoint.persistProject(token, project2);
        List<ProjectDto> sortedProjects = projectEndpoint.findAllProjectsSorted(token, "creationDate");
        Assert.assertEquals("fff", sortedProjects.get(0).getName());
        Assert.assertEquals("ddd", sortedProjects.get(1).getName());
        Assert.assertEquals("ccc", sortedProjects.get(2).getName());
    }

    @Test
    public void givenExistingUnsortedProjects_whenSortProjectsByStatus_thenProjectsSorted() throws Exception {
        final @NotNull ProjectDto project = new ProjectDto();
        project.setName("fff");
        project.setStatus(Status.IN_PROGRESS);
        projectEndpoint.persistProject(token, project);
        final @NotNull ProjectDto project1 = new ProjectDto();
        project1.setName("ddd");
        project1.setStatus(Status.SCHEDULED);
        projectEndpoint.persistProject(token, project1);
        final @NotNull ProjectDto project2 = new ProjectDto();
        project2.setName("ccc");
        project2.setStatus(Status.READY);
        projectEndpoint.persistProject(token, project2);
        List<ProjectDto> sortedProjects = projectEndpoint.findAllProjectsSorted(token, "status");
        Assert.assertEquals("fff", sortedProjects.get(0).getName());
        Assert.assertEquals("ccc", sortedProjects.get(1).getName());
        Assert.assertEquals("ddd", sortedProjects.get(2).getName());
    }

    @Test
    public void givenExistingProject_whenRemoveProject_thenProjectRemoved() throws Exception {
        final @NotNull ProjectDto project = new ProjectDto();
        final @NotNull String projectId = UUID.randomUUID().toString();
        project.setId(projectId);
        project.setName("gggg");
        projectEndpoint.persistProject(token, project);
        projectEndpoint.removeProject(token, project.getId());
        final @NotNull ProjectDto foundProject = projectEndpoint.findProjectByName(token, "gggg").stream().findFirst().orElse(null);
        Assert.assertNull(foundProject);
    }

    @Test
    public void givenExistingProjects_whenRemoveAllProjects_thenProjectsRemoved() throws Exception {
        final @NotNull ProjectDto project = new ProjectDto();
        project.setName("gggg");
        projectEndpoint.persistProject(token, project);
        final @NotNull ProjectDto project1 = new ProjectDto();
        project1.setName("ggggf");
        projectEndpoint.persistProject(token, project1);
        projectEndpoint.removeAllProjects(adminToken);
        final @NotNull List<ProjectDto> foundTasks = projectEndpoint.findAllProjects(token);
        Assert.assertTrue(foundTasks.isEmpty());
    }

    @Test
    public void givenExistingProject_whenMergeProject_thenProjectUpdated() throws Exception_Exception {
        final @NotNull ProjectDto project = new ProjectDto();
        final @NotNull String projectId = UUID.randomUUID().toString();
        project.setId(projectId);
        project.setName("dddd");
        project.setDescription("ffff");
        projectEndpoint.persistProject(token, project);
        final @NotNull ProjectDto foundProject = projectEndpoint.findOneProject(token, projectId);
        foundProject.setDescription("new");
        projectEndpoint.mergeProject(token, foundProject);
        final @NotNull ProjectDto foundUpdatedProject = projectEndpoint.findOneProject(token, projectId);
        Assert.assertEquals(foundUpdatedProject.getDescription(), "new");
    }

    @After
    public void tearDown() throws Exception_Exception {
        projectEndpoint.removeAllProjects(adminToken);
        sessionEndpoint.remove(token);
    }
}
