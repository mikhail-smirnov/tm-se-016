import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.ISessionEndpoint;
import ru.mismirnov.tm.api.endpoint.IUserEndpoint;
import ru.mismirnov.tm.api.endpoint.UserDto;
import ru.mismirnov.tm.endpoint.SessionEndpointService;
import ru.mismirnov.tm.endpoint.UserEndpointService;

public class SessionEndpointTest {

    private @NotNull ISessionEndpoint sessionEndpoint;
    private @NotNull IUserEndpoint userEndpoint;


    @Before
    public void setUp() {
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        userEndpoint = new UserEndpointService().getUserEndpointPort();
    }

    @Test
    public void givenUserCredentials_whenGetToken_thenSessionCreated() throws Exception_Exception {
        final @NotNull String token = sessionEndpoint.getToken("user", "user");
        Assert.assertNotNull(token);
        final @Nullable UserDto user = userEndpoint.findOneUser(token);
        Assert.assertNotNull(user);
    }
}
