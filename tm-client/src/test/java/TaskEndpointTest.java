import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.mismirnov.tm.api.endpoint.*;
import ru.mismirnov.tm.endpoint.SessionEndpointService;
import ru.mismirnov.tm.endpoint.TaskEndpointService;
import ru.mismirnov.tm.util.DateUtil;

import java.lang.Exception;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class TaskEndpointTest {

    private @NotNull ITaskEndpoint taskEndpoint;
    private @NotNull ISessionEndpoint sessionEndpoint;
    private @NotNull String token;
    private @NotNull String adminToken;

    @Before
    public void setUp() throws Exception_Exception {
        taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        token = sessionEndpoint.getToken("user", "user");
        adminToken = sessionEndpoint.getToken("admin", "admin");
    }

    @Test
    public void givenNewTask_whenAddTask_thenTaskAdded() throws Exception_Exception {
        final @NotNull TaskDto task = new TaskDto();
        task.setName("fffff");
        task.setDescription("dddddd");
        taskEndpoint.persistTask(token, task);
        Assert.assertNotNull(taskEndpoint.findTaskByName(token, "fffff"));
    }

    @Test
    public void givenExistingTasks_whenFindAllTasks_thenAllTasksFound() throws Exception_Exception {
        final @NotNull TaskDto task = new TaskDto();
        task.setName("fffff");
        task.setDescription("dddddd");
        taskEndpoint.persistTask(token, task);
        final @NotNull TaskDto task1 = new TaskDto();
        task1.setName("fffffs");
        task1.setDescription("dddddds");
        taskEndpoint.persistTask(token, task1);
        List<TaskDto> tasks = taskEndpoint.findAllTasks(token);
        Assert.assertTrue(tasks.size() == 2);
    }

    @Test
    public void givenExistingTask_whenTaskProjectByItsName_thenTaskFound() throws Exception_Exception {
        final @NotNull TaskDto task = new TaskDto();
        final @NotNull String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName("fffff");
        task.setDescription("dddddd");
        taskEndpoint.persistTask(token, task);
        final @NotNull TaskDto extractedTask = taskEndpoint.findTaskByName(token, "fffff").stream().findFirst().orElse(null);
        TaskDto foundTask = taskEndpoint.findOneTask(token, taskId);
        assertThat(foundTask).isEqualToComparingFieldByField(extractedTask);
    }

    @Test
    public void givenExistingTask_whenFindTaskByItsNDescription_thenTaskFound() throws Exception_Exception {
        final @NotNull TaskDto task = new TaskDto();
        final @NotNull String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName("fffff");
        task.setDescription("dddddd");
        taskEndpoint.persistTask(token, task);
        final @NotNull TaskDto extractedTask = taskEndpoint.findTaskByDescription(token, "dddddd").stream().findFirst().orElse(null);
        TaskDto foundTask = taskEndpoint.findOneTask(token, taskId);
        assertThat(foundTask).isEqualToComparingFieldByField(extractedTask);
    }

    @Test
    public void givenExistingUnsortedTasks_whenSortTasksByBeginDate_thenTasksSorted() throws Exception {
        final @NotNull TaskDto task = new TaskDto();
        task.setName("fff");
        task.setBeginDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2015")));
        taskEndpoint.persistTask(token, task);
        final @NotNull TaskDto task1 = new TaskDto();
        task1.setName("ddd");
        task1.setBeginDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2023")));
        taskEndpoint.persistTask(token, task1);
        final @NotNull TaskDto task2 = new TaskDto();
        task2.setName("ccc");
        task2.setBeginDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2016")));
        taskEndpoint.persistTask(token, task2);
        List<TaskDto> sortedTasks = taskEndpoint.findAllTasksSorted(token, "beginDate");
        Assert.assertEquals("fff", sortedTasks.get(0).getName());
        Assert.assertEquals("ccc", sortedTasks.get(1).getName());
        Assert.assertEquals("ddd", sortedTasks.get(2).getName());
    }

    @Test
    public void givenExistingUnsortedTasks_whenSortTasksByEndDate_thenTasksSorted() throws Exception {
        final @NotNull TaskDto task = new TaskDto();
        task.setName("fff");
        task.setEndDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2016")));
        taskEndpoint.persistTask(token, task);
        final @NotNull TaskDto task1 = new TaskDto();
        task1.setName("ddd");
        task1.setEndDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2024")));
        taskEndpoint.persistTask(token, task1);
        final @NotNull TaskDto task2 = new TaskDto();
        task2.setName("ccc");
        task2.setEndDate(DateUtil.toXMLGregorianCalendar(DateUtil.formatDate("20.20.2018")));
        taskEndpoint.persistTask(token, task2);
        List<TaskDto> sortedTasks = taskEndpoint.findAllTasksSorted(token, "endDate");
        Assert.assertEquals("fff", sortedTasks.get(0).getName());
        Assert.assertEquals("ccc", sortedTasks.get(1).getName());
        Assert.assertEquals("ddd", sortedTasks.get(2).getName());
    }

    @Test
    public void givenExistingUnsortedTasks_whenSortTasksByCreationDate_thenTasksSorted() throws Exception {
        final @NotNull TaskDto task = new TaskDto();
        task.setName("fff");
        taskEndpoint.persistTask(token, task);
        Thread.sleep(1000);
        final @NotNull TaskDto task1 = new TaskDto();
        task1.setName("ddd");
        taskEndpoint.persistTask(token, task1);
        Thread.sleep(1000);
        final @NotNull TaskDto task2 = new TaskDto();
        task2.setName("ccc");
        taskEndpoint.persistTask(token, task2);
        List<TaskDto> sortedTasks = taskEndpoint.findAllTasksSorted(token, "creationDate");
        Assert.assertEquals("fff", sortedTasks.get(0).getName());
        Assert.assertEquals("ddd", sortedTasks.get(1).getName());
        Assert.assertEquals("ccc", sortedTasks.get(2).getName());
    }

    @Test
    public void givenExistingUnsortedTasks_whenSortTasksByStatus_thenTasksSorted() throws Exception {
        final @NotNull TaskDto task = new TaskDto();
        task.setName("fff");
        task.setStatus(Status.READY);
        taskEndpoint.persistTask(token, task);
        final @NotNull TaskDto task1 = new TaskDto();
        task1.setName("ddd");
        task1.setStatus(Status.IN_PROGRESS);
        taskEndpoint.persistTask(token, task1);
        final @NotNull TaskDto task2 = new TaskDto();
        task2.setName("ccc");
        task2.setStatus(Status.SCHEDULED);
        taskEndpoint.persistTask(token, task2);
        List<TaskDto> sortedTasks = taskEndpoint.findAllTasksSorted(token, "status");
        Assert.assertEquals("ddd", sortedTasks.get(0).getName());
        Assert.assertEquals("fff", sortedTasks.get(1).getName());
        Assert.assertEquals("ccc", sortedTasks.get(2).getName());
    }

    @Test
    public void givenExistingTask_whenRemoveTask_thenTaskRemoved() throws Exception {
        final @NotNull TaskDto task = new TaskDto();
        final @NotNull String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName("gggg");
        taskEndpoint.persistTask(token, task);
        taskEndpoint.removeTask(token, task.getId());
        final @NotNull TaskDto foundTask = taskEndpoint.findTaskByName(token, "gggg").stream().findFirst().orElse(null);
        Assert.assertNull(foundTask);
    }

    @Test
    public void givenExistingTasks_whenRemoveAllTasks_thenTasksRemoved() throws Exception {
        final @NotNull TaskDto task = new TaskDto();
        task.setName("gggg");
        taskEndpoint.persistTask(token, task);
        final @NotNull TaskDto task1 = new TaskDto();
        task1.setName("ggggf");
        taskEndpoint.persistTask(token, task1);
        taskEndpoint.removeAllTasks(adminToken);
        final @NotNull List<TaskDto> foundTasks = taskEndpoint.findAllTasks(token);
        Assert.assertTrue(foundTasks.isEmpty());
    }

    @Test
    public void givenExistingTask_whenMergeTask_thenTaskUpdated() throws Exception_Exception {
        final @NotNull TaskDto task = new TaskDto();
        final @NotNull String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName("dddd");
        task.setDescription("ffff");
        taskEndpoint.persistTask(token, task);
        final @NotNull TaskDto foundTask = taskEndpoint.findOneTask(token, taskId);
        foundTask.setDescription("new");
        taskEndpoint.mergeTask(token, foundTask);
        final @NotNull TaskDto foundUpdatedTask = taskEndpoint.findOneTask(token, taskId);
        Assert.assertEquals(foundUpdatedTask.getDescription(), "new");
    }

    @After
    public void tearDown() throws Exception_Exception {
        taskEndpoint.removeAllTasks(adminToken);
        sessionEndpoint.remove(token);
    }
}
