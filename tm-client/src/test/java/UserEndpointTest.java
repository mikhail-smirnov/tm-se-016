import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.mismirnov.tm.api.endpoint.Exception_Exception;
import ru.mismirnov.tm.api.endpoint.ISessionEndpoint;
import ru.mismirnov.tm.api.endpoint.IUserEndpoint;
import ru.mismirnov.tm.api.endpoint.UserDto;
import ru.mismirnov.tm.endpoint.SessionEndpointService;
import ru.mismirnov.tm.endpoint.UserEndpointService;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class UserEndpointTest {

    private @NotNull IUserEndpoint userEndpoint;
    private @NotNull ISessionEndpoint sessionEndpoint;
    private @NotNull String token;
    private @NotNull String adminToken;


    @Before
    public void setUp() throws Exception_Exception {
        userEndpoint = new UserEndpointService().getUserEndpointPort();
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        token = sessionEndpoint.getToken("user", "user");
        adminToken = sessionEndpoint.getToken("admin", "admin");
    }

    @Test
    public void givenNewUser_whenAddUserAndRemoveUser_thenUserRemoved() throws Exception_Exception {
        final @NotNull UserDto user = new UserDto();
        final @NotNull String userId = UUID.randomUUID().toString();
        user.setId(userId);
        user.setName("fffff");
        user.setLogin("login");
        userEndpoint.persistUser(token, user);
        userEndpoint.removeUser(token, userId);
        final @Nullable UserDto foundUser = userEndpoint.findUserByLogin(token, "login");
        Assert.assertNull(foundUser);
    }

    @Test
    public void givenNewUser_whenAddUser_thenUserAdded() throws Exception_Exception {
        final @NotNull UserDto user = new UserDto();
        final @NotNull String userId = UUID.randomUUID().toString();
        user.setId(userId);
        user.setName("fffff");
        user.setLogin("hhhh");
        userEndpoint.persistUser(token, user);
        Assert.assertNotNull(userEndpoint.findOneUserById(adminToken, user.getId()));
        userEndpoint.removeUser(adminToken, userId);
    }

    @Test
    public void givenExistingUsers_whenFindAllUsers_thenAllUsersFound() throws Exception_Exception {
        List<UserDto> users = userEndpoint.findAllUsers(adminToken);
        Assert.assertTrue(users.size() == 2);
    }

    @Test
    public void givenExistingUser_whenFindUserByItsLogin_thenUserFound() throws Exception_Exception {
        final @NotNull UserDto user = new UserDto();
        final @NotNull String userId = UUID.randomUUID().toString();
        user.setId(userId);
        user.setLogin("fffff");
        user.setPassword("dddd");
        userEndpoint.persistUser(token, user);
        final @NotNull UserDto extractedUser = userEndpoint.findUserByLogin(token, "fffff");
        UserDto foundUser = userEndpoint.findOneUserById(adminToken, userId);
        assertThat(foundUser).isEqualToComparingFieldByField(extractedUser);
        userEndpoint.removeUser(adminToken, userId);
    }

    @After
    public void tearDown() throws Exception_Exception {
        sessionEndpoint.remove(token);
    }
}
