package ru.mismirnov.tm.api;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface FactoryLocator {

    @NotNull EntityManager getEntityManager() throws Exception;
}
