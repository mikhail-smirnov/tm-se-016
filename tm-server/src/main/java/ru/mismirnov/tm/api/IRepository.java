package ru.mismirnov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.mismirnov.tm.entity.AbstractEntity;
import ru.mismirnov.tm.exception.EntityDuplicateException;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll() throws Exception;

    @NotNull
    E findOneProject(final @NotNull String id) throws Exception;

    void remove(final @NotNull String id) throws Exception;

    E persistProject(final @NotNull E e) throws EntityDuplicateException, Exception;

    E mergeProject(final @NotNull E e) throws Exception;

    void removeAll() throws Exception;
}
