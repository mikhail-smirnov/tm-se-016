package ru.mismirnov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.entity.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    @Nullable
    List<E> findAll() throws Exception;

    @Nullable
    E findOne(final @Nullable String id) throws Exception;

    void remove(final @Nullable String id) throws Exception;

    E persist(final @Nullable E e) throws Exception;

    void merge(final @Nullable E e) throws Exception;

    void removeAll() throws Exception;

//    void load(@Nullable List<E> entitiesToLoad);
}