package ru.mismirnov.tm.api;

import ru.mismirnov.tm.api.project.IProjectService;
import ru.mismirnov.tm.api.property.IPropertyService;
import ru.mismirnov.tm.api.session.ISessionService;
import ru.mismirnov.tm.api.task.ITaskService;
import ru.mismirnov.tm.api.user.IUserService;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    ISessionService getSessionService();

    IPropertyService getPropertyService();
}
