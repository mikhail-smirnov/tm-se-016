package ru.mismirnov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.dto.ProjectDto;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @Nullable ProjectDto persistProject(@Nullable String token, @Nullable ProjectDto projectDto) throws Exception;

    @Nullable List<ProjectDto> findAllProjects(@Nullable String token) throws Exception;

    @Nullable ProjectDto findOneProject(@Nullable String token, @Nullable String projectId) throws Exception;

    @Nullable List<ProjectDto> findProjectByName(@Nullable String token, @Nullable String name) throws Exception;

    void removeProject(@Nullable String token, @Nullable String id) throws Exception;

    void removeAllProjects(@Nullable String token) throws Exception;

    void mergeProject(@Nullable String token, @Nullable ProjectDto projectDto) throws Exception;

    @Nullable List<ProjectDto> findProjectByDescription(@Nullable String token, @Nullable String name) throws Exception;

    @Nullable List<ProjectDto> findAllProjectsSorted(@Nullable String token, final @Nullable String sortFlag) throws Exception;
}
