package ru.mismirnov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    String getToken(@Nullable String login, @Nullable String password) throws Exception;

    @Nullable void remove(@Nullable String token) throws Exception;
}
