package ru.mismirnov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.dto.TaskDto;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @Nullable List<TaskDto> findAllTasks(@Nullable String token) throws Exception;

    @Nullable TaskDto findOneTask(@Nullable String token, @Nullable String taskId) throws Exception;

    void persistTask(@Nullable String token, @Nullable TaskDto taskDto) throws Exception;

    void mergeTask(@Nullable String token, @Nullable TaskDto taskDto) throws Exception;

    @Nullable List<TaskDto> findTaskByName(@Nullable String token, @Nullable String name) throws Exception;

    void removeTask(@Nullable String token, @Nullable String id) throws Exception;

    void removeAllTasks(@Nullable String token) throws Exception;

    void removeTasks(@Nullable String token, @Nullable List<TaskDto> tasksToRemove) throws Exception;

    @Nullable List<TaskDto> findTaskByDescription(@Nullable String token, @Nullable String name) throws Exception;

    @Nullable List<TaskDto> findAllTasksSorted(@Nullable String token, final @Nullable String sortFlag) throws Exception;
}
