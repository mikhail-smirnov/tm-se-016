package ru.mismirnov.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.dto.UserDto;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @Nullable List<UserDto> findAllUsers(@Nullable String token) throws Exception;

    @Nullable UserDto findOneUser(@Nullable String token) throws Exception;

    @Nullable UserDto findOneUserById(@Nullable String token, @Nullable String userId) throws Exception;

    @Nullable UserDto persistUser(@Nullable String token, @Nullable UserDto userDto) throws Exception;

    void mergeUser(@Nullable String token, @Nullable UserDto userDto) throws Exception;

    void registerDefaultUsers() throws Exception;

    @Nullable UserDto findUserByLogin(@Nullable String token, @Nullable String login) throws Exception;

    void removeAllUsers(@Nullable String token) throws Exception;

    void removeUser(@Nullable String token, @Nullable String userId) throws Exception;
}
