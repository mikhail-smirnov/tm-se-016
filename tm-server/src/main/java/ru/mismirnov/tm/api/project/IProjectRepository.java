package ru.mismirnov.tm.api.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {
    void persist(@NotNull Project project);

    void merge(@NotNull Project project);

    @Nullable Project findOne(@NotNull String userId, @NotNull String id);

    @Nullable Project findOne(@NotNull String id);

    @NotNull List<Project> findAll();

    @NotNull List<Project> findAll(@NotNull String userId);

    @NotNull void removeAll();

    @NotNull void removeAll(@NotNull String userId);

    @NotNull void remove(@NotNull String userId, @NotNull String id) throws Exception;

    @NotNull void remove(@NotNull String id) throws Exception;

    @NotNull List<Project> findByName(@NotNull String userId, @NotNull String name);

    @NotNull List<Project> findByDescription(@NotNull String userId, @NotNull String description);

    @NotNull List<Project> findAllSorted(@NotNull String userId, @NotNull String sortFlag);
}
