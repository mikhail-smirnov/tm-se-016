package ru.mismirnov.tm.api.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.IService;
import ru.mismirnov.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    List<Project> findAll(final @Nullable String userId) throws Exception;

    void removeAll(final @Nullable String userId) throws Exception;

    @Nullable
    List<Project> findByName(final @Nullable String userId, final @Nullable String name) throws Exception;

    void remove(final @Nullable String userId, final @Nullable String id) throws Exception;

    @Nullable
    List<Project> findByDescription(final @Nullable String userId, final @Nullable String name) throws Exception;

    @Nullable Project findOne(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull List<Project> findAllProjectsSortedByDate(final @Nullable String userId, final @Nullable String sortFlag) throws Exception;

//    @NotNull List<Project> findAllProjectsSortedByStatus(@Nullable String userId) throws Exception;

    void load(final @Nullable List<Project> entitiesToLoad) throws Exception;
}
