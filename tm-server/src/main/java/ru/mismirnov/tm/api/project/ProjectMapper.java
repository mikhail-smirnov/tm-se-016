package ru.mismirnov.tm.api.project;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jetbrains.annotations.NotNull;
import ru.mismirnov.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;

public interface ProjectMapper {


    @NotNull
    @Select("SELECT * FROM app_project")
    List<Project> findAllProjects() throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId}")
    List<Project> findAllProjectsByUserId(@NotNull String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project WHERE id = #{id}")
    Project findOneProject(@NotNull String id) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} and id = #{id}")
    Project findOneProjectByUserId(@NotNull @Param("userId") String userId,
                                   @NotNull @Param("id") String id) throws Exception;

    @Select("INSERT INTO app_project " +
            "VALUES (#{id}, #{name}, #{description}, #{beginDate}, #{endDate}, #{creationDate}, #{userId}, #{status})")
    Project persistProject(Project project) throws Exception;

    @Select("UPDATE app_project " +
            "SET name = #{name}, description = #{description}, " +
            "beginDate = #{beginDate}, endDate = #{endDate},  creationDate = #{creationDate}, userId = #{userId}, " +
            "status = #{status} WHERE id = #{id}")
    Project mergeProject(@NotNull Project project) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} AND name = #{name}")
    List<Project> findProjectByName(@NotNull @Param("userId") String userId,
                                    @NotNull @Param("name") String name) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} AND description = #{description}")
    List<Project> findProjectByDescription(@NotNull @Param("userId") String userId,
                                           @NotNull @Param("description") String description) throws SQLException;

    @Delete("DELETE FROM app_project WHERE id = #{id}")
    void removeProject(@NotNull String id) throws SQLException;


    @Delete("DELETE FROM app_project WHERE userId = #{userId} AND id = #{id}")
    void removeProjectByUserId(@NotNull @Param("userId") String userId, @NotNull @Param("id") String projectId) throws SQLException;


    @Delete("DELETE FROM app_project")
    void removeAllProjects() throws SQLException;

    @Delete("DELETE FROM app_project WHERE userId = #{userId}")
    void removeAllProjectsByUserId(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} ORDER BY ${sortFlag}")
    List<Project> findAllProjectsSortedByDate(@NotNull @Param("userId") String userId,
                                              @NotNull @Param("sortFlag") String sortFlag) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} ORDER BY status")
    List<Project> findAllProjectsSortedByStatus(@NotNull @Param("userId") String userId) throws Exception;

}
