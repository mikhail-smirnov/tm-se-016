package ru.mismirnov.tm.api.property;

import org.jetbrains.annotations.NotNull;

import java.util.Properties;

public interface IPropertyService {

    Properties getProperties();

    String getJdbcDriver() throws Exception;

    @NotNull String getJdbcUrl() throws Exception;

    @NotNull String getJdbcUsername() throws Exception;

    @NotNull String getJdbcPassword() throws Exception;
}
