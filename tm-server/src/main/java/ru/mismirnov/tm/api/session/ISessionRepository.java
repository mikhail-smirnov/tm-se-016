package ru.mismirnov.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {
    void persist(@NotNull Session session);

    void merge(@NotNull Session session);

    @Nullable Session findOne(@NotNull String userId, @NotNull String id);

    @Nullable Session findOne(@NotNull String id);

    @NotNull List<Session> findAll();

    @NotNull List<Session> findAll(@NotNull String userId);

    @NotNull void removeAll();

    @NotNull void removeAll(@NotNull String userId);

    @NotNull void remove(@NotNull String userId, @NotNull String id) throws Exception;

    @NotNull void remove(@NotNull String id) throws Exception;
}
