package ru.mismirnov.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.entity.Session;
import ru.mismirnov.tm.enumerated.Role;

public interface ISessionService {

    @Nullable
    Session create(@Nullable String login, @Nullable String password) throws Exception;

    @Nullable Session validate(@Nullable String token) throws Exception;

    Session validate(@NotNull String token, @NotNull Role... roles) throws Exception;

    @Nullable
    Session findOne(@Nullable String userId, @Nullable String id) throws Exception;

    void remove(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    String getToken(@Nullable String login, @Nullable String password) throws Exception;
}
