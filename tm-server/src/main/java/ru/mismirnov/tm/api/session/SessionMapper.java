package ru.mismirnov.tm.api.session;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jetbrains.annotations.NotNull;
import ru.mismirnov.tm.entity.Session;

import java.sql.SQLException;
import java.util.List;

public interface SessionMapper {

    @NotNull
    @Select("SELECT * FROM app_session")
    List<Session> findAllSessions() throws Exception;

    @NotNull
    @Select("SELECT * FROM app_session WHERE id = #{id}")
    Session findOneSession(@NotNull String id) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_session WHERE userId = #{userId} and id = #{id}")
    Session findOneSessionByUserId(@NotNull String userId, @NotNull String id) throws Exception;

    @Insert("INSERT INTO app_session VALUES (#{id}, #{userId}, #{role}, #{signature}, #{creationDate})")
    void persistSession(Session session) throws Exception;

    @Select("UPDATE app_session " +
            "SET userId = #{userId}, role = #{role}, " +
            "signature = #{signature}, creationDate = #{creationDate}")
    Session mergeSession(@NotNull Session session) throws Exception;

    @Delete("DELETE FROM app_session WHERE id = #{id}")
    void removeSession(@NotNull String id) throws SQLException;

    @Delete("DELETE FROM app_session WHERE userId = #{userId} AND id = #{id}")
    void removeSessionByUserId(@NotNull @Param("userId") String userId,
                               @NotNull @Param("id") String id) throws SQLException;

    @Delete("DELETE FROM app_session")
    void removeAllSessions() throws SQLException;
}
