package ru.mismirnov.tm.api.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {
    void persist(@NotNull Task task);

    void merge(@NotNull Task task);

    @Nullable Task findOne(@NotNull String userId, @NotNull String id);

    @Nullable Task findOne(@NotNull String id);

    @NotNull List<Task> findAll();

    @NotNull List<Task> findAll(@NotNull String userId);

    @NotNull void removeAll();

    @NotNull void removeAll(@NotNull String userId);

    @NotNull void remove(@NotNull String userId, @NotNull String id) throws Exception;

    @NotNull void remove(@NotNull String id) throws Exception;

    @NotNull List<Task> findByName(@NotNull String userId, @NotNull String name);

    @NotNull List<Task> findByDescription(@NotNull String userId, @NotNull String description);

    @NotNull List<Task> findAllSorted(@NotNull String userId, @NotNull String sortFlag);

    @NotNull List<Task> findTasksByProjectId(@NotNull String userId, @NotNull String projectId);
}
