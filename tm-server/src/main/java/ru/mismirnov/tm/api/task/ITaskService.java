package ru.mismirnov.tm.api.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.IService;
import ru.mismirnov.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void removeAll(final @Nullable String userId) throws Exception;

    @Nullable
    List<Task> findAll(final @Nullable String userId) throws Exception;

    @Nullable
    List<Task> findByName(final @Nullable String userId, final @Nullable String name) throws Exception;

    void remove(final @Nullable String userId, final @Nullable String id) throws Exception;

    void removeTasks(final @Nullable String userId, final @Nullable List<Task> tasksToRemove) throws Exception;

    @Nullable
    List<Task> findByDescription(final @Nullable String userId, final @Nullable String name) throws Exception;

    @Nullable Task findOne(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull List<Task> findAllTasksSortedByDate(@Nullable String userId, @Nullable String sortFlag) throws Exception;

    @NotNull List<Task> findTasksByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

//    @NotNull List<Task> findAllTasksSortedByStatus(@Nullable String userId) throws Exception;

    void load(final @Nullable List<Task> entitiesToLoad) throws Exception;
}

