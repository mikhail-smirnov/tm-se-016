package ru.mismirnov.tm.api.task;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jetbrains.annotations.NotNull;
import ru.mismirnov.tm.entity.Task;

import java.sql.SQLException;
import java.util.List;

public interface TaskMapper {

    @NotNull
    @Select("SELECT * FROM app_task")
    List<Task> findAllTasks() throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId}")
    List<Task> findAllTasksByUserId(@NotNull String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE id = #{id}")
    Task findOneTask(@NotNull String id) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} and id = #{id}")
    Task findOneTaskByUserId(@NotNull @Param("userId") String userId,
                             @NotNull @Param("id") String id) throws Exception;

    @Select("INSERT INTO app_task " +
            "VALUES (#{id}, #{name}, #{description}, #{beginDate}, #{endDate}, #{creationDate}, #{projectId}, #{userId}, #{status})")
    Task persistTask(Task task) throws Exception;

    @Select("UPDATE app_task " +
            "SET name = #{name}, description = #{description}, " +
            "beginDate = #{beginDate}, endDate = #{endDate},  creationDate = #{creationDate}, projectId = #{projectId}, userId = #{userId}, " +
            "status = #{status} WHERE id = #{id}")
    Task mergeTask(@NotNull Task task) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} AND name = #{name}")
    List<Task> findTaskByName(@NotNull @Param("userId") String userId,
                              @NotNull @Param("name") String name) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} AND description = #{description}")
    List<Task> findTaskByDescription(@NotNull @Param("userId") String userId,
                                     @NotNull @Param("description") String description) throws SQLException;

    @Delete("DELETE FROM app_task WHERE id = #{id}")
    void removeTask(@NotNull String id) throws SQLException;

    @Delete("DELETE FROM app_task WHERE userId = #{userId} AND id = #{id}")
    void removeTaskByUserId(@NotNull @Param("userId") String userId,
                            @NotNull @Param("id") String id) throws SQLException;


    @Delete("DELETE FROM app_task")
    void removeAllTasks() throws SQLException;

    @Delete("DELETE FROM app_task WHERE userId = #{userId}")
    void removeAllTasksByUserId(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} ORDER BY ${sortFlag}")
    List<Task> findAllTasksSortedByDate(@NotNull @Param("userId") String userId,
                                        @NotNull @Param("sortFlag") String sortFlag) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} ORDER BY status")
    List<Task> findAllTasksSortedByStatus(@NotNull @Param("userId") String userId) throws Exception;
}

