package ru.mismirnov.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.entity.User;

import java.util.List;

public interface IUserRepository {
    void persist(@NotNull User user);

    void merge(@NotNull User user);

    @Nullable User findOne(@NotNull String id);

    @NotNull List<User> findAll();

    @NotNull void removeAll();

    @NotNull void remove(@NotNull String id) throws Exception;

    @NotNull User findByLogin(@NotNull String login);
}
