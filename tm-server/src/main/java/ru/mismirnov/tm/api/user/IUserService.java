package ru.mismirnov.tm.api.user;

import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.IService;
import ru.mismirnov.tm.entity.User;

import java.util.List;

public interface IUserService extends IService<User> {

//    void changePassword(final @Nullable String userId, final @Nullable String password) throws NoSuchAlgorithmException;

    @Nullable
    User findByLogin(final @Nullable String login) throws Exception;

    void setDefault() throws Exception;

//    @Nullable
//    User authorize(final @Nullable String login, final @Nullable String password);

    void load(final @Nullable List<User> entitiesToLoad) throws Exception;
}

