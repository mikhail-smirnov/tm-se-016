package ru.mismirnov.tm.api.user;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface UserMapper {

    @NotNull
    @Select("SELECT * FROM app_user")
    List<User> findAllUsers() throws Exception;

    @NotNull
    @Select("SELECT * FROM app_user WHERE id = #{id}")
    User findOneUser(@NotNull String id) throws Exception;


    @Select("INSERT INTO app_user VALUES (#{id}, #{name}, #{login}, #{password}, #{role})")
    User persistUser(User user) throws Exception;

    @Select("UPDATE app_user " +
            "SET name = #{name}, description = #{login}, " +
            "password = #{password}, role = #{role} WHERE id = #{id}")
    User mergeUser(@NotNull User user) throws Exception;


    @Delete("DELETE FROM app_user WHERE id = #{id}")
    void removeUser(@NotNull String id) throws SQLException;


    @Delete("DELETE FROM app_user")
    void removeAllUsers() throws SQLException;

    @Nullable
    @Select("SELECT * FROM app_user WHERE login = #{login}")
    User findUserByLogin(final @NotNull String login) throws Exception;
}

