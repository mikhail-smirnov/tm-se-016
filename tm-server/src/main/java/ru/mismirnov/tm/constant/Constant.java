package ru.mismirnov.tm.constant;

import org.jetbrains.annotations.NotNull;

import java.io.File;

public final class Constant {

    public final static @NotNull String SALT = "fddghhddd";
    public final static @NotNull Integer CYCLE = 100;
    public final static @NotNull Integer SESSION_LIFE_TIME = 1000000;
    public final static @NotNull String PROPERTIES = "database.properties";
    public static final @NotNull String FILE_LOCATION = System.getProperty("user.dir") + File.separator
            + "target" + File.separator + "task-manager" + File.separator + "data" + File.separator;

}
