package ru.mismirnov.tm.context;

import lombok.Getter;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.FactoryLocator;
import ru.mismirnov.tm.api.ServiceLocator;
import ru.mismirnov.tm.api.endpoint.IProjectEndpoint;
import ru.mismirnov.tm.api.endpoint.ISessionEndpoint;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.api.endpoint.IUserEndpoint;
import ru.mismirnov.tm.api.project.IProjectService;
import ru.mismirnov.tm.api.property.IPropertyService;
import ru.mismirnov.tm.api.session.ISessionService;
import ru.mismirnov.tm.api.task.ITaskService;
import ru.mismirnov.tm.api.user.IUserService;
import ru.mismirnov.tm.endpoint.ProjectEndpoint;
import ru.mismirnov.tm.endpoint.SessionEndpoint;
import ru.mismirnov.tm.endpoint.TaskEndpoint;
import ru.mismirnov.tm.endpoint.UserEndpoint;
import ru.mismirnov.tm.entity.Project;
import ru.mismirnov.tm.entity.Session;
import ru.mismirnov.tm.entity.Task;
import ru.mismirnov.tm.entity.User;
import ru.mismirnov.tm.service.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@Getter
public final class Bootstrap implements ServiceLocator, FactoryLocator {

    private static final @NotNull String LINE_SEPARATOR = System.getProperty("line.separator");
    private final @NotNull IProjectService projectService = new ProjectService(this);
    private final @NotNull ITaskService taskService = new TaskService(this);
    private final @NotNull IUserService userService = new UserService(this);
    private final @NotNull ISessionService sessionService = new SessionService(this, this);
    private final @NotNull ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    private final @NotNull IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    private final @NotNull IUserEndpoint userEndpoint = new UserEndpoint(this);
    private final @NotNull ISessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService);
    private final @NotNull IPropertyService propertyService = new PropertyService();
    private @Nullable EntityManagerFactory entityManagerFactory;

    public Bootstrap() throws IOException {
    }

    private void init() {
        Endpoint.publish("http://localhost:9090/projectendpoint?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:9090/taskendpoint?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:9090/userendpoint?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:9090/sessionendpoint?wsdl", sessionEndpoint);
        try {
            entityManagerFactory = getEntityManagerFactory();
            userService.setDefault();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        init();
    }

    @NotNull
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    public EntityManagerFactory getEntityManagerFactory() throws Exception {
        final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getJdbcDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getJdbcUrl());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getJdbcUsername());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getJdbcPassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, "org.hibernate.dialect.PostgreSQLDialect");
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, "true");
        settings.put(Environment.USE_QUERY_CACHE, "true");
        settings.put(Environment.USE_MINIMAL_PUTS, "true");
        settings.put("hibernate.cache.hazelcast.use_lite_member", "true");
        settings.put(Environment.CACHE_REGION_PREFIX, "task-manager");
        settings.put(Environment.CACHE_REGION_FACTORY, "com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory");
        settings.put(Environment.CACHE_PROVIDER_CONFIG, "hazelcast.xml");
        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}