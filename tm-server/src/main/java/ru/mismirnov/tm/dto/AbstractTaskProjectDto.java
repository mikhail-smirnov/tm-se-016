package ru.mismirnov.tm.dto;

import org.jetbrains.annotations.NotNull;
import ru.mismirnov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Date;

public class AbstractTaskProjectDto extends AbstractDto {

    @Column(updatable = false)
    protected @NotNull Date creationDate = new Date();

    @Enumerated(value = EnumType.STRING)
    protected @NotNull Status status = Status.SCHEDULED;
}
