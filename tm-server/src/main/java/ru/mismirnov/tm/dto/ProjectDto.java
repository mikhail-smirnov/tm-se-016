package ru.mismirnov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.ServiceLocator;
import ru.mismirnov.tm.api.task.ITaskService;
import ru.mismirnov.tm.api.user.IUserService;
import ru.mismirnov.tm.entity.Project;
import ru.mismirnov.tm.entity.User;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDto extends AbstractTaskProjectDto {

    private @Nullable String name;

    private @Nullable String description;

    private @Nullable Date beginDate;

    private @Nullable Date endDate;

    private @Nullable String userId;

    @NotNull
    public Project transformFromDtoToProjectEntity(final @NotNull ServiceLocator serviceLocator) throws Exception {
        final @NotNull Project project = new Project();
        final @NotNull IUserService userService = serviceLocator.getUserService();
        final @NotNull ITaskService taskService = serviceLocator.getTaskService();
        final @Nullable User user = userService.findOne(userId);
        if (user == null) throw new Exception("There is no such user ");
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setBeginDate(beginDate);
        project.setEndDate(endDate);
        project.setStatus(status);
        project.setCreationDate(creationDate);
        project.setUser(user);
        project.setTasks(taskService.findTasksByProjectId(userId, id));
        return project;
    }

    @NotNull
    public ProjectDto transformFromProjectEntityToProjectDto(final @NotNull Project project) {
        this.id = project.getId();
        this.name = project.getName();
        this.description = project.getDescription();
        this.beginDate = project.getBeginDate();
        this.endDate = project.getEndDate();
        this.creationDate = project.getCreationDate();
        this.status = project.getStatus();
        this.userId = project.getUser().getId();
        return this;
    }
}
