package ru.mismirnov.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.ServiceLocator;
import ru.mismirnov.tm.api.user.IUserService;
import ru.mismirnov.tm.entity.Session;
import ru.mismirnov.tm.entity.User;
import ru.mismirnov.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SessionDto extends AbstractDto {

    private @Nullable String userId;

    private @Nullable Role role;

    private @Nullable String signature;

    @JsonIgnore
    private long creationDate = new Date().getTime();

    @NotNull
    public Session transformFromSessionDtoToSessionEntity(final @NotNull ServiceLocator serviceLocator) throws Exception {
        final @NotNull Session session = new Session();
        final @NotNull IUserService userService = serviceLocator.getUserService();
        final @Nullable User user = userService.findOne(userId);
        if (user == null) throw new Exception("There is no such user ");
        session.setId(id);
        session.setUser(user);
        session.setCreationDate(creationDate);
        session.setRole(role);
        session.setSignature(signature);
        return session;
    }

    @NotNull
    public SessionDto transformFromSessionEntityToSessionDto(final @NotNull Session session) {
        this.id = session.getId();
        this.signature = session.getSignature();
        this.role = session.getRole();
        this.creationDate = session.getCreationDate();
        this.userId = session.getUser().getId();
        return this;
    }
}
