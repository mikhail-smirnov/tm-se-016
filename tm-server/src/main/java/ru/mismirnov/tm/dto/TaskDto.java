package ru.mismirnov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.ServiceLocator;
import ru.mismirnov.tm.api.project.IProjectService;
import ru.mismirnov.tm.api.user.IUserService;
import ru.mismirnov.tm.entity.Project;
import ru.mismirnov.tm.entity.Task;
import ru.mismirnov.tm.entity.User;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class TaskDto extends AbstractTaskProjectDto {

    private @Nullable String name;

    private @Nullable String description;

    private @Nullable Date beginDate;

    private @Nullable Date endDate;

    private @Nullable String projectId;

    private @Nullable String userId;

    public Task transformFromTaskDtoToTaskEntity(final @NotNull ServiceLocator serviceLocator) throws Exception {
        final @NotNull Task task = new Task();
        final @NotNull IUserService userService = serviceLocator.getUserService();
        final @NotNull IProjectService projectService = serviceLocator.getProjectService();
        final @Nullable User user = userService.findOne(userId);
        final @Nullable Project project = projectService.findOne(userId, projectId);
        if (user == null) throw new Exception("There is no such user");
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setBeginDate(beginDate);
        task.setEndDate(endDate);
        task.setCreationDate(creationDate);
        task.setStatus(status);
        task.setUser(user);
        task.setProject(project);
        return task;
    }

    @NotNull
    public TaskDto transformFromTaskEntityToTaskDto(final @NotNull Task task) {
        this.id = task.getId();
        this.name = task.getName();
        this.description = task.getDescription();
        this.beginDate = task.getBeginDate();
        this.endDate = task.getEndDate();
        this.creationDate = task.getCreationDate();
        this.status = task.getStatus();
        this.userId = task.getUser().getId();
        if (task.getProject() != null) {
            this.projectId = task.getProject().getId();
        }
        return this;
    }
}
