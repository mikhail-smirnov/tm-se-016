package ru.mismirnov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.ServiceLocator;
import ru.mismirnov.tm.api.project.IProjectService;
import ru.mismirnov.tm.api.task.ITaskService;
import ru.mismirnov.tm.entity.User;
import ru.mismirnov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public class UserDto extends AbstractDto {

    @Nullable
    private String name;

    @Nullable
    private String login;

    @Nullable
    private String password;

    @NotNull
    private Role role = Role.USER;

    @NotNull
    public User transformUserDtoToUserEntity(final @NotNull ServiceLocator serviceLocator) throws Exception {
        final @NotNull User user = new User();
        final @NotNull IProjectService projectService = serviceLocator.getProjectService();
        final @NotNull ITaskService taskService = serviceLocator.getTaskService();
        user.setId(id);
        user.setName(name);
        user.setLogin(login);
        user.setPassword(password);
        user.setRole(role);
        user.setProjects(projectService.findAll(id));
        user.setTasks(taskService.findAll(id));
        return user;
    }

    @Nullable
    public UserDto transformUserEntityToUserDto(final @Nullable User user) {
        if (user == null || user.isEmpty()) return null;
        this.id = user.getId();
        this.name = user.getName();
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.role = user.getRole();
        return this;
    }
}
