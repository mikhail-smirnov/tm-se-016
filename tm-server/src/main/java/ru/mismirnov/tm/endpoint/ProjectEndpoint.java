package ru.mismirnov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.ServiceLocator;
import ru.mismirnov.tm.api.endpoint.IProjectEndpoint;
import ru.mismirnov.tm.dto.ProjectDto;
import ru.mismirnov.tm.entity.Project;
import ru.mismirnov.tm.entity.Session;
import ru.mismirnov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@WebService(endpointInterface = "ru.mismirnov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint implements IProjectEndpoint {

    @NotNull
    private ServiceLocator serviceLocator;

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    public ProjectDto persistProject(final @Nullable String token, final @Nullable ProjectDto projectDto) throws Exception {
        if (token == null || token.isEmpty()) return null;
        if (projectDto == null) return null;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return null;
        projectDto.setUserId(session.getUser().getId());
        final @NotNull Project project = projectDto.transformFromDtoToProjectEntity(serviceLocator);
        return new ProjectDto()
                .transformFromProjectEntityToProjectDto(serviceLocator.getProjectService().persist(project));
    }

    @Nullable
    @Override
    public List<ProjectDto> findAllProjects(final @Nullable String token) throws Exception {
        if (token == null || token.isEmpty()) return null;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return null;
        return transformProjectsEntitiesToProjectsDto(serviceLocator.getProjectService().findAll(session.getUser().getId()));
    }

    @Nullable
    @Override
    public ProjectDto findOneProject(final @Nullable String token, final @Nullable String projectId) throws Exception {
        if (token == null || token.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return null;
        return new ProjectDto()
                .transformFromProjectEntityToProjectDto(serviceLocator.getProjectService().findOne(session.getUser().getId(), projectId));
    }

    @Nullable
    @Override
    public List<ProjectDto> findProjectByName(final @Nullable String token, final @Nullable String name) throws Exception {
        if (token == null || token.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return null;
        return transformProjectsEntitiesToProjectsDto(serviceLocator.getProjectService().findByName(session.getUser().getId(), name));
    }


    @Override
    public void removeProject(final @Nullable String token, final @Nullable String id) throws Exception {
        if (token == null || token.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return;
        serviceLocator.getProjectService().remove(session.getUser().getId(), id);
    }

    @Override
    public void removeAllProjects(final @Nullable String token) throws Exception {
        if (token == null || token.isEmpty()) return;
        serviceLocator.getSessionService().validate(token, Role.ADMIN);
        serviceLocator.getProjectService().removeAll();
    }


    @Nullable
    @Override
    public void mergeProject(final @Nullable String token, final @Nullable ProjectDto projectDto) throws Exception {
        if (token == null || token.isEmpty()) return;
        if (projectDto == null) return;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        projectDto.setUserId(session.getUser().getId());
        final @NotNull Project project = projectDto.transformFromDtoToProjectEntity(serviceLocator);
        serviceLocator.getProjectService().merge(project);
    }

    @Nullable
    @Override
    public List<ProjectDto> findProjectByDescription(final @Nullable String token, final @Nullable String name) throws Exception {
        if (token == null || token.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return null;
        return transformProjectsEntitiesToProjectsDto(serviceLocator
                .getProjectService().findByDescription(session.getUser().getId(), name));
    }

    @Nullable
    @Override
    public List<ProjectDto> findAllProjectsSorted(final @Nullable String token, final @Nullable String sortFlag) throws Exception {
        if (token == null || token.isEmpty()) return null;
        if (sortFlag == null || sortFlag.isEmpty()) return null;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return null;
        return transformProjectsEntitiesToProjectsDto(serviceLocator
                .getProjectService().findAllProjectsSortedByDate(session.getUser().getId(), sortFlag));
    }


    @Nullable
    @WebMethod(exclude = true)
    public List<ProjectDto> transformProjectsEntitiesToProjectsDto(final @Nullable List<Project> projects) {
        if (projects == null || projects.isEmpty()) return null;
        final @NotNull List<ProjectDto> projectsDto = new ArrayList<>();
        for (ListIterator<Project> iterator = projects.listIterator(); iterator.hasNext(); ) {
            final @NotNull Project project = iterator.next();
            projectsDto.add(new ProjectDto().transformFromProjectEntityToProjectDto(project));
        }
        return projectsDto;
    }
}
