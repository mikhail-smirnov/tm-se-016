package ru.mismirnov.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.endpoint.ISessionEndpoint;
import ru.mismirnov.tm.api.session.ISessionService;
import ru.mismirnov.tm.entity.Session;

import javax.jws.WebService;

@Setter
@WebService(endpointInterface = "ru.mismirnov.tm.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint implements ISessionEndpoint {

    @NotNull
    private ISessionService sessionService;

    public SessionEndpoint(final @NotNull ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Override
    public String getToken(final @Nullable String login, final @Nullable String password) throws Exception {
        if (login == null || login.isEmpty()) return null;
        return sessionService.getToken(login, password);
    }

    @Override
    public void remove(final @Nullable String token) throws Exception {
        if (token == null || token.isEmpty()) return;
        final @Nullable Session session = sessionService.validate(token);
        if (session == null) return;
        sessionService.remove(session.getUser().getId(), session.getId());
    }
}

