package ru.mismirnov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.ServiceLocator;
import ru.mismirnov.tm.api.endpoint.ITaskEndpoint;
import ru.mismirnov.tm.dto.TaskDto;
import ru.mismirnov.tm.entity.Session;
import ru.mismirnov.tm.entity.Task;
import ru.mismirnov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@WebService(endpointInterface = "ru.mismirnov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint implements ITaskEndpoint {

    @NotNull
    private ServiceLocator serviceLocator;

    public TaskEndpoint(final @NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    public List<TaskDto> findAllTasks(final @Nullable String token) throws Exception {
        if (token == null || token.isEmpty()) return null;
        serviceLocator.getSessionService().validate(token);
        return transformTasksEntitiesToTasksDto(serviceLocator.getTaskService().findAll());
    }

    @Nullable
    @Override
    public TaskDto findOneTask(final @Nullable String token, final @Nullable String taskId) throws Exception {
        if (token == null || token.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return null;
        return new TaskDto().transformFromTaskEntityToTaskDto(serviceLocator
                .getTaskService().findOne(session.getUser().getId(), taskId));
    }

    @Override
    public void persistTask(final @Nullable String token, final @Nullable TaskDto taskDto) throws Exception {
        if (token == null || token.isEmpty()) return;
        if (taskDto == null) return;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return;
        taskDto.setUserId(session.getUser().getId());
        final @NotNull Task task = taskDto.transformFromTaskDtoToTaskEntity(serviceLocator);
        serviceLocator.getTaskService().persist(task);
    }

    @Override
    public void mergeTask(final @Nullable String token, final @Nullable TaskDto taskDto) throws Exception {
        if (token == null || token.isEmpty()) return;
        if (taskDto == null) return;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return;
        taskDto.setUserId(session.getUser().getId());
        final @NotNull Task task = taskDto.transformFromTaskDtoToTaskEntity(serviceLocator);
        serviceLocator.getTaskService().merge(task);
    }

    @Nullable
    @Override
    public List<TaskDto> findTaskByName(final @Nullable String token, final @Nullable String name) throws Exception {
        if (token == null || token.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return null;
        return transformTasksEntitiesToTasksDto(serviceLocator
                .getTaskService().findByName(session.getUser().getId(), name));
    }


    @Override
    public void removeTask(final @Nullable String token, final @Nullable String id) throws Exception {
        if (token == null || token.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return;
        serviceLocator.getTaskService().remove(session.getUser().getId(), id);
    }

    @Override
    public void removeAllTasks(final @Nullable String token) throws Exception {
        if (token == null || token.isEmpty()) return;
        serviceLocator.getSessionService().validate(token, Role.ADMIN);
        serviceLocator.getTaskService().removeAll();
    }

    @Override
    public void removeTasks(final @Nullable String token, final @Nullable List<TaskDto> tasksDtoToRemove) throws Exception {
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return;
        List<Task> tasksToRemove = transformTasksDtoToTasksEntities(tasksDtoToRemove);
        serviceLocator.getTaskService().removeTasks(session.getUser().getId(), tasksToRemove);
    }


    @Nullable
    @Override
    public List<TaskDto> findTaskByDescription(final @Nullable String token, @Nullable final String name) throws Exception {
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return null;
        return transformTasksEntitiesToTasksDto(serviceLocator
                .getTaskService().findByDescription(session.getUser().getId(), name));
    }

    @Nullable
    @Override
    public List<TaskDto> findAllTasksSorted(final @Nullable String token, final @Nullable String sortFlag) throws Exception {
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return null;
        return transformTasksEntitiesToTasksDto(serviceLocator
                .getTaskService().findAllTasksSortedByDate(session.getUser().getId(), sortFlag));
    }

    @Nullable
    @WebMethod(exclude = true)
    public List<TaskDto> transformTasksEntitiesToTasksDto(final @Nullable List<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return null;
        final @NotNull List<TaskDto> tasksDto = new ArrayList<>();
        for (ListIterator<Task> iterator = tasks.listIterator(); iterator.hasNext(); ) {
            final @NotNull Task task = iterator.next();
            tasksDto.add(new TaskDto().transformFromTaskEntityToTaskDto(task));
        }
        return tasksDto;
    }

    @Nullable
    @WebMethod(exclude = true)
    public List<Task> transformTasksDtoToTasksEntities(final @Nullable List<TaskDto> tasksDto) throws Exception {
        if (tasksDto == null) return null;
        final @NotNull List<Task> tasks = new ArrayList<>();
        for (ListIterator<TaskDto> iterator = tasksDto.listIterator(); iterator.hasNext(); ) {
            final @NotNull TaskDto taskDto = iterator.next();
            tasks.add(taskDto.transformFromTaskDtoToTaskEntity(serviceLocator));
        }
        return tasks;
    }
}
