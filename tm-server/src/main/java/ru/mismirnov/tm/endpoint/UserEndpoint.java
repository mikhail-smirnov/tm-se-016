package ru.mismirnov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.ServiceLocator;
import ru.mismirnov.tm.api.endpoint.IUserEndpoint;
import ru.mismirnov.tm.dto.UserDto;
import ru.mismirnov.tm.entity.Session;
import ru.mismirnov.tm.entity.User;
import ru.mismirnov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@WebService(endpointInterface = "ru.mismirnov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint implements IUserEndpoint {

    @NotNull
    private ServiceLocator serviceLocator;

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    public List<UserDto> findAllUsers(final @Nullable String token) throws Exception {
        if (token == null || token.isEmpty()) return null;
        serviceLocator.getSessionService().validate(token, Role.ADMIN);
        return transformUsersEntitiesToUsersDto(serviceLocator.getUserService().findAll());
    }

    @Nullable
    @Override
    public UserDto findOneUser(final @Nullable String token) throws Exception {
        if (token == null || token.isEmpty()) return null;
        final @Nullable Session session = serviceLocator.getSessionService().validate(token);
        if (session == null) return null;
        return new UserDto().transformUserEntityToUserDto(serviceLocator
                .getUserService().findOne(session.getUser().getId()));
    }

    @Nullable
    @Override
    public UserDto findOneUserById(final @Nullable String token, final @Nullable String userId) throws Exception {
        if (token == null || token.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        serviceLocator.getSessionService().validate(token);
        return new UserDto().transformUserEntityToUserDto(serviceLocator
                .getUserService().findOne(userId));
    }

    @Nullable
    @Override
    public UserDto persistUser(final @Nullable String token, final @Nullable UserDto userDto) throws Exception {
        if (token == null || token.isEmpty()) return null;
        if (userDto == null) return null;
        serviceLocator.getSessionService().validate(token);
        final @NotNull User user = userDto.transformUserDtoToUserEntity(serviceLocator);
        return new UserDto().transformUserEntityToUserDto(serviceLocator.getUserService().persist(user));
    }

    @Override
    public void mergeUser(final @Nullable String token, final @Nullable UserDto userDto) throws Exception {
        if (token == null || token.isEmpty()) return;
        if (userDto == null) return;
        serviceLocator.getSessionService().validate(token);
        final @NotNull User user = userDto.transformUserDtoToUserEntity(serviceLocator);
        serviceLocator.getUserService().merge(user);
    }

    @Override
    public void registerDefaultUsers() throws Exception {
        serviceLocator.getUserService().setDefault();
    }

    @Nullable
    @Override
    public UserDto findUserByLogin(final @Nullable String token, final @Nullable String login) throws Exception {
        if (token == null || token.isEmpty()) return null;
        if (login == null || login.isEmpty()) return null;
        serviceLocator.getSessionService().validate(token);
        return new UserDto().transformUserEntityToUserDto(serviceLocator
                .getUserService().findByLogin(login));
    }

    @Override
    public void removeAllUsers(final @Nullable String token) throws Exception {
        if (token == null || token.isEmpty()) return;
        serviceLocator.getSessionService().validate(token);
        serviceLocator.getUserService().removeAll();
    }

    @Override
    public void removeUser(final @Nullable String token, final @Nullable String userId) throws Exception {
        if (token == null || token.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        serviceLocator.getSessionService().validate(token);
        serviceLocator.getUserService().remove(userId);
    }

    @Nullable
    @WebMethod(exclude = true)
    public List<UserDto> transformUsersEntitiesToUsersDto(final @Nullable List<User> users) {
        if (users == null || users.isEmpty()) return null;
        final @NotNull List<UserDto> usersDto = new ArrayList<>();
        for (ListIterator<User> iterator = users.listIterator(); iterator.hasNext(); ) {
            final @NotNull User user = iterator.next();
            usersDto.add(new UserDto().transformUserEntityToUserDto(user));
        }
        return usersDto;
    }
}

