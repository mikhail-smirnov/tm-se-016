package ru.mismirnov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.mismirnov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractTaskProject extends AbstractEntity implements Serializable {

    @Column(updatable = false)
    private @NotNull Date creationDate = new Date();

    @Enumerated(value = EnumType.STRING)
    private @NotNull Status status = Status.SCHEDULED;
}

