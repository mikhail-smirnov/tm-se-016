package ru.mismirnov.tm.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity(name = "Project")
@Table(name = "app_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Project extends AbstractTaskProject implements Serializable {

    @Column(nullable = false)
    private @Nullable String name;

    @Column(columnDefinition = "TEXT")
    private @Nullable String description;

    private @Nullable Date beginDate;

    private @Nullable Date endDate;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonManagedReference(value = "project-tasks")
    private @Nullable List<Task> tasks = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    @JsonBackReference(value = "user-projects")
    private @NotNull User user;

    @JsonIgnore
    public boolean isEmpty() {
        for (@NotNull Field field : this.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                if (field.get(this) != null) {
                    return false;
                }
            } catch (Exception e) {
                System.out.println("Exception occurred in processing");
            }
        }
        return true;
    }
}
