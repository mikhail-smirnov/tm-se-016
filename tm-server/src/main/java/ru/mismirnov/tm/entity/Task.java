package ru.mismirnov.tm.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;


@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity(name = "Task")
@Table(name = "app_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Task extends AbstractTaskProject implements Serializable {

    @Column(nullable = false)
    private @Nullable String name;

    @Column(columnDefinition = "TEXT")
    private @Nullable String description;

    private @Nullable Date beginDate;

    private @Nullable Date endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "taskId")
    @JsonBackReference(value = "project-tasks")
    private @Nullable Project project;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    @JsonBackReference(value = "user-tasks")
    private @Nullable User user;

    @JsonIgnore
    public boolean isEmpty() {
        for (@NotNull Field field : this.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                if (field.get(this) != null) {
                    return false;
                }
            } catch (Exception e) {
                System.out.println("Exception occurred in processing");
            }
        }
        return true;
    }
}
