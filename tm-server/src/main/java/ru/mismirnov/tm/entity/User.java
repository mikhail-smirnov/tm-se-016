package ru.mismirnov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.hibernate.annotations.Cache;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.enumerated.Role;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Cacheable
@NoArgsConstructor
@Entity(name = "User")
@Table(name = "app_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class User extends AbstractEntity implements Serializable {

    private @Nullable String name;

    @Column(unique = true, nullable = false)
    private @Nullable String login;

    private @Nullable String password;

    @Enumerated(value = EnumType.STRING)
    private @NotNull Role role;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonManagedReference(value = "user-projects")
    private List<Project> projects = new ArrayList<>();

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonManagedReference(value = "user-tasks")
    private List<Task> tasks = new ArrayList<>();

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonManagedReference(value = "user-sessions")
    private List<Session> sessions = new ArrayList<>();

    public User(final @Nullable String name, final @Nullable String login, final @Nullable String password, final @Nullable Role role) {
        this.name = name;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    @JsonIgnore
    public boolean isEmpty() {
        for (@NotNull Field field : this.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                if (field.get(this) != null) {
                    return false;
                }
            } catch (Exception e) {
                System.out.println("Exception occurred in processing");
            }
        }
        return true;
    }
}
