package ru.mismirnov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {
    USER("User"),
    ADMIN("Administrator");

    private final @NotNull String displayName;

    Role(final @NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}

