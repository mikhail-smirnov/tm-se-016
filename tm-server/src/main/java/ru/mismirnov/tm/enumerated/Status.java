package ru.mismirnov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public enum Status {

    SCHEDULED("Запланировано", 0), IN_PROGRESS("В процессе выполнения", 1), READY("Выполнено", 2);

    private final String displayName;
    private final int index;

    Status(final @NotNull String displayName, final int index) {
        this.displayName = displayName;
        this.index = index;
    }

    public static Status getValueFromString(final @NotNull String text) {
        for (@NotNull final Status status : Status.values()) {
            if (status.getDisplayName().equalsIgnoreCase(text)) {
                return status;
            }
        }
        return null;
    }
}
