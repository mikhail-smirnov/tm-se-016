package ru.mismirnov.tm.exception;

public final class AccessForbiddenException extends Exception {

    public AccessForbiddenException() {
        super("Access forbidden");
    }
}
