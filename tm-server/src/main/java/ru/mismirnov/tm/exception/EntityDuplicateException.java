package ru.mismirnov.tm.exception;

public final class EntityDuplicateException extends Exception {

    public EntityDuplicateException() {
        super("Entity is already in place");
    }
}
