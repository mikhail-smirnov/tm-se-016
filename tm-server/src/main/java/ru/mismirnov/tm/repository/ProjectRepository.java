package ru.mismirnov.tm.repository;

import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.project.IProjectRepository;
import ru.mismirnov.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.ListIterator;

public class ProjectRepository implements IProjectRepository {

    @NotNull
    private final EntityManager entityManager;

    public ProjectRepository(final @NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(final @NotNull Project project) {
        entityManager.persist(project);
    }

    @Override
    public void merge(final @NotNull Project project) {
        entityManager.merge(project);
    }

    @Nullable
    @Override
    public Project findOne(final @NotNull String userId, final @NotNull String id) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE user.id = :userId AND id = :id", Project.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("userId", userId).setParameter("id", id).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Project findOne(final @NotNull String id) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE id = :id", Project.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("id", id).getResultList().stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return entityManager.createQuery("SELECT p FROM Project p", Project.class).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll(final @NotNull String userId) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE user.id = :userId", Project.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public void removeAll() {
        for (ListIterator<Project> iterator = findAll().listIterator(); iterator.hasNext(); ) {
            final @Nullable Project project = iterator.next();
            entityManager.remove(project);
        }
    }

    @NotNull
    @Override
    public void removeAll(final @NotNull String userId) {
        for (ListIterator<Project> iterator = findAll(userId).listIterator(); iterator.hasNext(); ) {
            final @NotNull Project project = iterator.next();
            entityManager.remove(project);
        }
    }

    @NotNull
    @Override
    public void remove(final @NotNull String userId, @NotNull final String id) throws Exception {
        final @Nullable Project project = findOne(userId, id);
        if (project == null) throw new Exception();
        entityManager.remove(project);
    }

    @NotNull
    @Override
    public void remove(final @NotNull String id) throws Exception {
        final @Nullable Project project = findOne(id);
        if (project == null) throw new Exception();
        entityManager.remove(project);
    }

    @NotNull
    @Override
    public List<Project> findByName(final @NotNull String userId, final @NotNull String name) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE user.id = :userId AND name = :name", Project.class)
                .setParameter("userId", userId).setParameter("name", name).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findByDescription(final @NotNull String userId, final @NotNull String description) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE user.id = :userId AND description = :description", Project.class)
                .setParameter("userId", userId).setParameter("description", description).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAllSorted(final @NotNull String userId, final @NotNull String sortFlag) {
        return entityManager.createQuery("SELECT p FROM Project p WHERE user.id = :userId ORDER BY " + sortFlag)
                .setParameter("userId", userId).getResultList();
    }
}
