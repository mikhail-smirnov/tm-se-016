package ru.mismirnov.tm.repository;

import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.session.ISessionRepository;
import ru.mismirnov.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.ListIterator;

public class SessionRepository implements ISessionRepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionRepository(final @NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(final @NotNull Session session) {
        entityManager.persist(session);
    }

    @Override
    public void merge(final @NotNull Session session) {
        entityManager.merge(session);
    }

    @Nullable
    @Override
    public Session findOne(final @NotNull String userId, final @NotNull String id) {
        return entityManager.createQuery("SELECT s FROM Session s WHERE user.id = :userId AND id = :id", Session.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("userId", userId).setParameter("id", id)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Session findOne(final @NotNull String id) {
        return entityManager.createQuery("SELECT s FROM Session s WHERE id = :id", Session.class)
                .setParameter("id", id).getResultList().stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return entityManager.createQuery("SELECT s FROM Session s", Session.class).getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll(final @NotNull String userId) {
        return entityManager.createQuery("SELECT s FROM Session s WHERE user.id = :userId", Session.class)
                .setParameter("userId", userId).getResultList();
    }

    @Override
    public void removeAll() {
        for (ListIterator<Session> iterator = findAll().listIterator(); iterator.hasNext(); ) {
            final @Nullable Session session = iterator.next();
            entityManager.remove(session);
        }
    }

    @Override
    public void removeAll(final @NotNull String userId) {
        for (ListIterator<Session> iterator = findAll(userId).listIterator(); iterator.hasNext(); ) {
            final @NotNull Session session = iterator.next();
            entityManager.remove(session);
        }
    }

    @Override
    public void remove(final @NotNull String userId, @NotNull final String id) throws Exception {
        final @Nullable Session session = findOne(userId, id);
        if (session == null) throw new Exception();
        entityManager.remove(session);
    }

    @Override
    public void remove(final @NotNull String id) throws Exception {
        final @Nullable Session session = findOne(id);
        if (session == null) throw new Exception();
        entityManager.remove(session);
    }
}
