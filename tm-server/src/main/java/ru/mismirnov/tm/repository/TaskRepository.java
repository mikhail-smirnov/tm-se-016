package ru.mismirnov.tm.repository;

import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.task.ITaskRepository;
import ru.mismirnov.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.ListIterator;

public class TaskRepository implements ITaskRepository {

    @NotNull
    private final EntityManager entityManager;

    public TaskRepository(final @NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(final @NotNull Task task) {
        entityManager.persist(task);
    }

    @Override
    public void merge(final @NotNull Task task) {
        entityManager.merge(task);
    }

    @Nullable
    @Override
    public Task findOne(final @NotNull String userId, final @NotNull String id) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE user.id = :userId AND id = :id", Task.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("userId", userId).setParameter("id", id).getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public Task findOne(final @NotNull String id) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE id = :id", Task.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("id", id).getResultList().stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("SELECT t FROM Task t", Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(final @NotNull String userId) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE user.id = :userId", Task.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public void removeAll() {
        for (ListIterator<Task> iterator = findAll().listIterator(); iterator.hasNext(); ) {
            final @Nullable Task task = iterator.next();
            entityManager.remove(task);
        }
    }

    @NotNull
    @Override
    public void removeAll(final @NotNull String userId) {
        for (ListIterator<Task> iterator = findAll(userId).listIterator(); iterator.hasNext(); ) {
            final @NotNull Task task = iterator.next();
            entityManager.remove(task);
        }
    }

    @NotNull
    @Override
    public void remove(final @NotNull String userId, @NotNull final String id) throws Exception {
        final @Nullable Task task = findOne(userId, id);
        if (task == null) throw new Exception();
        entityManager.remove(task);
    }

    @NotNull
    @Override
    public void remove(final @NotNull String id) throws Exception {
        final @Nullable Task task = findOne(id);
        if (task == null) throw new Exception();
        entityManager.remove(task);
    }

    @NotNull
    @Override
    public List<Task> findByName(final @NotNull String userId, final @NotNull String name) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE user.id = :userId AND name = :name", Task.class)
                .setParameter("userId", userId).setParameter("name", name).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findByDescription(final @NotNull String userId, final @NotNull String description) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE user.id = :userId AND description = :description", Task.class)
                .setParameter("userId", userId).setParameter("description", description).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAllSorted(final @NotNull String userId, final @NotNull String sortFlag) {
        return entityManager.createQuery("SELECT t FROM Task t WHERE user.id = :userId ORDER BY " + sortFlag)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT t FROM Task t " +
                "WHERE user.id = :userId AND project.id = :projectId ORDER BY creationDate", Task.class)
                .setParameter("userId", userId).setParameter("projectId", projectId).getResultList();
    }

}
