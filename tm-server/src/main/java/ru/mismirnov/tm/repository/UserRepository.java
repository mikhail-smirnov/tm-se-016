package ru.mismirnov.tm.repository;

import org.hibernate.annotations.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.user.IUserRepository;
import ru.mismirnov.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.ListIterator;

public class UserRepository implements IUserRepository {

    @NotNull
    private final EntityManager entityManager;

    public UserRepository(final @NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void persist(final @NotNull User user) {
        entityManager.persist(user);
    }

    @Override
    public void merge(final @NotNull User user) {
        entityManager.merge(user);
    }

    @Nullable
    @Override
    public User findOne(final @NotNull String id) {
        return entityManager.createQuery("SELECT u FROM User u WHERE id = :id", User.class)
                .setHint(QueryHints.CACHEABLE, true)
                .setParameter("id", id).getResultList().stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Override
    public void removeAll() {
        for (ListIterator<User> iterator = findAll().listIterator(); iterator.hasNext(); ) {
            final @Nullable User user = iterator.next();
            entityManager.remove(user);
        }
    }

    @Override
    public void remove(final @NotNull String id) throws Exception {
        final @Nullable User user = findOne(id);
        if (user == null) throw new Exception();
        entityManager.remove(user);
    }

    @Nullable
    @Override
    public User findByLogin(final @NotNull String login) {
        return entityManager.createQuery("SELECT u FROM User u WHERE login = :login", User.class)
                .setParameter("login", login).getResultList().stream().findFirst().orElse(null);
    }
}
