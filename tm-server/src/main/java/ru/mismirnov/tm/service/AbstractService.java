package ru.mismirnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.FactoryLocator;
import ru.mismirnov.tm.api.IService;
import ru.mismirnov.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final FactoryLocator factoryLocator;

    protected AbstractService(final @NotNull FactoryLocator factoryLocator) {
        this.factoryLocator = factoryLocator;
    }


    @Nullable
    @Override
    public abstract List<E> findAll() throws Exception;

    @Nullable
    @Override
    public abstract E findOne(final @Nullable String id) throws Exception;

    @Override
    public abstract void remove(final @Nullable String id) throws Exception;

    @Override
    public abstract E persist(final @Nullable E entity) throws Exception;

    @Override
    public abstract void merge(final @Nullable E entity) throws Exception;

    @Override
    public abstract void removeAll() throws Exception;
}

