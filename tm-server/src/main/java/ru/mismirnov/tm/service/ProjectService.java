package ru.mismirnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.FactoryLocator;
import ru.mismirnov.tm.api.project.IProjectRepository;
import ru.mismirnov.tm.api.project.IProjectService;
import ru.mismirnov.tm.entity.Project;
import ru.mismirnov.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull FactoryLocator factoryLocator) {
        super(factoryLocator);
    }

    @Nullable
    @Override
    public List<Project> findAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        final @NotNull List<Project> projects = projectRepository.findAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

    @Nullable
    @Override
    public List<Project> findAll() throws Exception {
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        final @NotNull List<Project> projects = projectRepository.findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

    @Nullable
    @Override
    public List<Project> findByName(final @Nullable String userId, final @Nullable String name) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        final @NotNull List<Project> projects = projectRepository.findByName(userId, name);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

    public Project persist(final @Nullable Project project) throws Exception {
        if (project == null || project.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.persist(project);
        entityManager.getTransaction().commit();
        entityManager.close();
        return project;
    }


    public void merge(final @Nullable Project project) throws Exception {
        if (project == null || project.isEmpty()) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.merge(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.remove(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.removeAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() throws Exception {
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public List<Project> findByDescription(final @Nullable String userId, final @Nullable String name) throws Exception {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        final @Nullable List<Project> projects = projectRepository.findByDescription(userId, name);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }


    @Nullable
    @Override
    public Project findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        final @Nullable Project project = projectRepository.findOne(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return project;
    }

    @Nullable
    public Project findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        final @Nullable Project project = projectRepository.findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return project;
    }

    @Override
    public @NotNull List<Project> findAllProjectsSortedByDate(final @Nullable String userId, final @Nullable String sortFlag) throws Exception {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IProjectRepository projectRepository = new ProjectRepository(entityManager);
        final @Nullable List<Project> projects = projectRepository.findAllSorted(userId, sortFlag);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projects;
    }

    @Override
    public void load(@Nullable List<Project> entitiesToLoad) throws Exception {
        for (ListIterator<Project> iterator = entitiesToLoad.listIterator(); iterator.hasNext(); ) {
            Project project = iterator.next();
            persist(project);
        }
    }
}