package ru.mismirnov.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.property.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static ru.mismirnov.tm.constant.Constant.PROPERTIES;

@Getter
public final class PropertyService implements IPropertyService {

    @NotNull
    final Properties properties = new Properties();

    @NotNull
    final InputStream inputStream = ProjectService.class.getClassLoader().getResourceAsStream(PROPERTIES);

    public PropertyService() throws IOException {
        properties.load(inputStream);
    }

    @Override
    public Properties getProperties() {
        return properties;
    }

    @NotNull
    @Override
    public String getJdbcDriver() throws Exception {
        @Nullable final String driver = properties.getProperty("db.driver");
        if (driver == null || driver.isEmpty()) throw new Exception("Config file not found");
        return driver;
    }

    @NotNull
    @Override
    public String getJdbcUrl() throws Exception {
        @Nullable final String url = properties.getProperty("db.host");
        if (url == null || url.isEmpty()) throw new Exception("Config file not found");
        return url;
    }

    @NotNull
    @Override
    public String getJdbcUsername() throws Exception {
        @Nullable final String username = properties.getProperty("db.login");
        if (username == null || username.isEmpty()) throw new Exception("Invalid config file");
        return username;
    }

    @NotNull
    @Override
    public String getJdbcPassword() throws Exception {
        @Nullable final String password = properties.getProperty("db.password");
        if (password == null || password.isEmpty()) throw new Exception("Invalid config file");
        return password;
    }
}
