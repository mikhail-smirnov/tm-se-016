package ru.mismirnov.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.FactoryLocator;
import ru.mismirnov.tm.api.ServiceLocator;
import ru.mismirnov.tm.api.session.ISessionRepository;
import ru.mismirnov.tm.api.session.ISessionService;
import ru.mismirnov.tm.api.user.IUserRepository;
import ru.mismirnov.tm.dto.SessionDto;
import ru.mismirnov.tm.entity.Session;
import ru.mismirnov.tm.entity.User;
import ru.mismirnov.tm.enumerated.Role;
import ru.mismirnov.tm.exception.AccessForbiddenException;
import ru.mismirnov.tm.repository.SessionRepository;
import ru.mismirnov.tm.repository.UserRepository;
import ru.mismirnov.tm.util.AESUtil;
import ru.mismirnov.tm.util.PasswordHashUtil;
import ru.mismirnov.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static ru.mismirnov.tm.constant.Constant.*;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull ServiceLocator serviceLocator;

    public SessionService(final @NotNull FactoryLocator factoryLocator, final @NotNull ServiceLocator serviceLocator) {
        super(factoryLocator);
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public Session create(final @Nullable String login, final @Nullable String password) throws Exception {
        if (login == null || login.isEmpty()) return (new Session());
        if (password == null || password.isEmpty()) return (new Session());
        final @NotNull String hashedPassword = PasswordHashUtil.getMD5(password);
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        final @Nullable IUserRepository userRepository = new UserRepository(entityManager);
        final @Nullable User user = userRepository.findByLogin(login);
        if (!user.getPassword().equals(hashedPassword)) return (new Session());
        if (user == null) return (new Session());
        @NotNull final Session session = new Session();
        session.setRole(user.getRole());
        session.setUser(user);
        session.setSignature(SignatureUtil
                .sign(new SessionDto().transformFromSessionEntityToSessionDto(session), SALT, CYCLE));
        persist(session);
        return session;
    }

    @NotNull
    @Override
    public Session validate(final @Nullable String token) throws Exception {
        if (token == null) throw new AccessForbiddenException();
        final @NotNull SessionDto userSession = decryptSession(token);
        if (userSession.getUserId() == null) throw new AccessForbiddenException();
        final @Nullable Session session = findOne(userSession.getId());
        if (session == null) throw new AccessForbiddenException();
        final @Nullable String signatureOfIncomingSession = SignatureUtil
                .sign(userSession, SALT, CYCLE);
        final @Nullable String signatureOfTempSession = SignatureUtil
                .sign(new SessionDto().transformFromSessionEntityToSessionDto(session), SALT, CYCLE);
        final boolean signatureCheck = signatureOfIncomingSession.equals(signatureOfTempSession);
        if (!signatureCheck) throw new AccessForbiddenException();
        final long sessionLifeTime = new Date().getTime() - session.getCreationDate();
        if (sessionLifeTime > SESSION_LIFE_TIME) throw new AccessForbiddenException();
        session.setSignature(signatureOfIncomingSession);
        return session;
    }

    @NotNull
    @Override
    public Session validate(final @Nullable String token, final @Nullable Role... roles) throws Exception {
        if (token == null) throw new AccessForbiddenException();
        @NotNull final SessionDto userSession = decryptSession(token);
        if (roles == null || !Arrays.asList(roles).contains(userSession.getRole()))
            throw new AccessForbiddenException();
        return validate(token);
    }

    @Nullable
    @Override
    public List<Session> findAll() throws Exception {
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ISessionRepository sessionRepository = new SessionRepository(entityManager);
        final @Nullable List<Session> sessions = sessionRepository.findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return sessions;
    }

    @Override
    public void merge(final @Nullable Session session) throws Exception {
        if (session == null) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ISessionRepository sessionRepository = new SessionRepository(entityManager);
        sessionRepository.merge(session);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() throws Exception {
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ISessionRepository sessionRepository = new SessionRepository(entityManager);
        sessionRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public Session findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ISessionRepository sessionRepository = new SessionRepository(entityManager);
        final @Nullable Session session = sessionRepository.findOne(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return session;
    }

    @Nullable
    @Override
    public Session findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ISessionRepository sessionRepository = new SessionRepository(entityManager);
        final @Nullable Session session = sessionRepository.findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return session;
    }


    @Override
    public void remove(final @Nullable String userId, String id) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ISessionRepository sessionRepository = new SessionRepository(entityManager);
        sessionRepository.remove(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ISessionRepository sessionRepository = new SessionRepository(entityManager);
        sessionRepository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public Session persist(@Nullable Session session) throws Exception {
        if (session == null) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ISessionRepository sessionRepository = new SessionRepository(entityManager);
        sessionRepository.persist(session);
        entityManager.getTransaction().commit();
        entityManager.close();
        return session;
    }

    private SessionDto decryptSession(@Nullable final String token) throws Exception {
        final @NotNull String key = serviceLocator.getPropertyService().getProperties().getProperty("token.key");
        final @NotNull String json = AESUtil.decrypt(token, key);
        final @NotNull ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        final @NotNull SessionDto sessionDto = mapper.readValue(json, SessionDto.class);
        return sessionDto;
    }

    private String encryptSession(@Nullable final SessionDto sessionDto) throws Exception {
        final @NotNull String key = serviceLocator.getPropertyService().getProperties().getProperty("token.key");
        final @NotNull ObjectMapper objectMapper = new ObjectMapper();
        final @NotNull String json = objectMapper.writeValueAsString(sessionDto);
        return AESUtil.encrypt(json, key);
    }

    @Nullable
    @Override
    public String getToken(@Nullable final String login, @Nullable final String password) throws Exception {
        final @Nullable Session session = create(login, password);
        return encryptSession(new SessionDto().transformFromSessionEntityToSessionDto(session));
    }
}
