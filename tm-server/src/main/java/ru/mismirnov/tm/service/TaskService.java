package ru.mismirnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.FactoryLocator;
import ru.mismirnov.tm.api.task.ITaskRepository;
import ru.mismirnov.tm.api.task.ITaskService;
import ru.mismirnov.tm.entity.Task;
import ru.mismirnov.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull FactoryLocator factoryLocator) {
        super(factoryLocator);
    }

    @Nullable
    @Override
    public List<Task> findAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        final @NotNull List<Task> tasks = taskRepository.findAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        final @NotNull List<Task> tasks = taskRepository.findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> findByName(final @Nullable String userId, final @Nullable String name) throws Exception {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        final @NotNull List<Task> tasks = taskRepository.findByName(userId, name);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    public Task persist(final @Nullable Task task) throws Exception {
        if (task == null || task.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.persist(task);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    public void merge(final @Nullable Task task) throws Exception {
        if (task == null || task.isEmpty()) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.merge(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void remove(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.remove(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void removeAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.removeAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void removeAll() throws Exception {
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeTasks(final @Nullable String userId, final @Nullable List<Task> tasksToRemove) throws Exception {
        if (tasksToRemove == null || tasksToRemove.isEmpty()) return;
        for (ListIterator<Task> iterator = tasksToRemove.listIterator(); iterator.hasNext(); ) {
            Task task = iterator.next();
            remove(task.getId());
        }
    }

    @Nullable
    @Override
    public List<Task> findByDescription(final @Nullable String userId, final @Nullable String description) throws Exception {
        if (description == null || description.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        final @NotNull List<Task> tasks = taskRepository.findByDescription(userId, description);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        final @Nullable Task task = taskRepository.findOne(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public Task findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        final @Nullable Task task = taskRepository.findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @Override
    public @NotNull List<Task> findAllTasksSortedByDate(final @Nullable String userId, final @Nullable String sortFlag) throws Exception {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        final @NotNull List<Task> tasks = taskRepository.findAllSorted(userId, sortFlag);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull ITaskRepository taskRepository = new TaskRepository(entityManager);
        final @NotNull List<Task> tasks = taskRepository.findTasksByProjectId(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @Override
    public void load(@Nullable List<Task> entitiesToLoad) throws Exception {
        for (ListIterator<Task> iterator = entitiesToLoad.listIterator(); iterator.hasNext(); ) {
            Task task = iterator.next();
            persist(task);
        }
    }
}

