package ru.mismirnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mismirnov.tm.api.FactoryLocator;
import ru.mismirnov.tm.api.user.IUserRepository;
import ru.mismirnov.tm.api.user.IUserService;
import ru.mismirnov.tm.entity.User;
import ru.mismirnov.tm.enumerated.Role;
import ru.mismirnov.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.ListIterator;

import static ru.mismirnov.tm.util.PasswordHashUtil.getMD5;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull FactoryLocator factoryLocator) {
        super(factoryLocator);
    }

    @NotNull
    public List<User> findAll() throws Exception {
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IUserRepository userRepository = new UserRepository(entityManager);
        final List<User> users = userRepository.findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return users;
    }

    @Nullable
    @Override
    public User findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IUserRepository userRepository = new UserRepository(entityManager);
        final @Nullable User user = userRepository.findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    @Override
    public void setDefault() throws Exception {
        User admin = new User();
        admin.setRole(Role.ADMIN);
        admin.setName("admin");
        admin.setLogin("admin");
        admin.setPassword(getMD5("admin"));
        persist(admin);

        User defaultUser = new User();
        defaultUser.setRole(Role.USER);
        defaultUser.setName("user");
        defaultUser.setLogin("user");
        defaultUser.setPassword(getMD5("user"));
        persist(defaultUser);
    }

    @Override
    public void load(@Nullable List<User> entitiesToLoad) throws Exception {
        for (ListIterator<User> iterator = entitiesToLoad.listIterator(); iterator.hasNext(); ) {
            User user = iterator.next();
            persist(user);
        }
    }

    @Nullable
    @Override
    public User findByLogin(final @Nullable String login) throws Exception {
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IUserRepository userRepository = new UserRepository(entityManager);
        final @Nullable User user = userRepository.findByLogin(login);
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IUserRepository userRepository = new UserRepository(entityManager);
        userRepository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public User persist(final @Nullable User user) throws Exception {
        if (user == null || user.isEmpty()) return null;
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IUserRepository userRepository = new UserRepository(entityManager);
        userRepository.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    public void merge(final @Nullable User user) throws Exception {
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IUserRepository userRepository = new UserRepository(entityManager);
        userRepository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void removeAll() throws Exception {
        final @NotNull EntityManager entityManager = factoryLocator.getEntityManager();
        entityManager.getTransaction().begin();
        final @NotNull IUserRepository userRepository = new UserRepository(entityManager);
        userRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }
}
