package ru.mismirnov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import static ru.mismirnov.tm.constant.Constant.PROPERTIES;

public final class ConnectionUtil {

    @NotNull
    public static Connection getConnection() throws Exception {

        @NotNull final Properties properties = new Properties();
        try (final InputStream inputStream = ConnectionUtil.class.getClassLoader().getResourceAsStream(PROPERTIES)) {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new Exception("Corrupted properties.");
        }
        Class.forName(properties.getProperty("db.driver"));
        return DriverManager.getConnection(properties.getProperty("db.host"), properties.getProperty("db.login"),
                properties.getProperty("db.password"));
    }
}